# ifndef FILESTREAM_HPP
# define FILESTREAM_HPP

# include <fcntl.h>
# include "Stream.hpp"

namespace io
{
	/**
	 * \brief File bufferization
	 *
	 * It's the same thing as io::Stream except that it works with files.
	*/
	class FileStream : public Stream
	{
	public :
		typedef std::function<void(FileStream *)> Callback;
		typedef std::function<void(FileStream *, Message *)> Recv_Callback;

		enum Mode { READ = O_RDONLY, WRITE = O_WRONLY | O_CREAT | O_TRUNC, APPEND = O_WRONLY | O_APPEND };

	private :
		std::string 				_fileName;

	// Coplien
		explicit FileStream(void);

	public :
	// Getters
		std::string const &			getFileName(void) const;

	// Coplien
		virtual ~FileStream(void);
		explicit FileStream(FileStream const & ref);
		FileStream const &		operator=(FileStream const & rhs);

		explicit FileStream(std::string const & fileName, Mode m = READ);

			/**
			 * \brief Receive hook.
			 * \note see Stream::onReceive()
			*/
		void						onReceive(Recv_Callback f);

			/**
			 * \brief Plug hook.
			 * \note see Stream::onPlug()
			*/
		void						onPlug(Callback f);

			/**
			 * \brief Unplug hook.
			 * \note see Stream::onUnplug()
			*/
		void						onUnplug(Callback f);

			/**
			 * \brief Close hook.
			 * \note see Stream::onClose()
			*/
		void						onClose(Callback f);

			/**
			 * \note see io::Stream::send(Message *, Callback)
			*/
		void						send(Message *m, Callback f = 0);

			/**
			 * \note see io::Stream::send(Message *, size_t, Callback)
			*/
		void						send(Message *m, size_t len, Callback f = 0);

			/**
			 * \note see io::Stream::send(Message *, size_t, size_t, Callback)
			*/
		void						send(Message *m, size_t start, size_t end, Callback f = 0);

			/**
			 * \note see io::Stream::send(char const * m, Callback f)
			*/
		void						send(char const * m, Callback f = 0);
	};

} /* namespace io */

#endif /* !FILESTREAM_HPP */
