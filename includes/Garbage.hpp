#ifndef GARBAGE_HPP
# define GARBAGE_HPP

# include <stddef.h>

namespace io
{
	/**
	 * \brief Collectable object, can be used by io::Collector for garbage collection (see io::GC global)
	 *
	 * Many objects in libIO are collectable.  \n
	 * They inherit from io::Garbage for this.  \n
	 *
	 *
	 * Any Garbage object has a virtual destructor that it used by io::GC.  \n
	 * On heap allocation, Garbage is automatically locked (see io::Collector class)  \n
	 * Garbage destructor call io::Collector::forget()  \n
	*/
	class Garbage
	{
	private :
		bool			_onStack;

	public :
		explicit Garbage(void);
		virtual ~Garbage(void);

		void *		operator new(size_t);
		bool		isOnStack(void) const;
	};
}

#endif /* !GARBAGE_HPP */
