# ifndef EXECSTREAM_HPP
# define EXECSTREAM_HPP

# include <unistd.h>
# include <signal.h>
# include <memory>
# include "internal/Env.hpp"
# include "Stream.hpp"

namespace io
{
	/**
	 * \brief Execution Bufferization
	 *
	 * It's the same thing as io::Stream except that it works with processes.
	 * If you want use onFork() callback, do it before call ExecStream::exec() function
	 *
	*/
	class ExecStream : public Stream
	{
	public :
		typedef std::function<void(ExecStream *)> Callback;
		typedef std::function<void(ExecStream *, Message *)> Recv_Callback;
		typedef std::function<void(ExecStream *, uint8_t, bool)> Exit_Callback;

	protected :
		pid_t						_pid;

	private :
		Callback					_forkCallback;
		Exit_Callback				_exitCallback;
		std::string const			_stringPath;
		std::string const			_stringArgv;
		char *						_path;
		char **						_argv;
		std::shared_ptr<utils::Env>	_env;

	// Private Static Utilities
		static char *	create_path(std::string const & path);
		static char **	create_argv(std::string const & argv);

	// Private Utilities
		void						triggerUnplugged(void);

	// Coplien
		explicit ExecStream(void);

	public :
		void						checkIfExited(void);
	// Getters
		pid_t						getPid(void) const;
		std::string const &			getPath(void) const;
		std::string const &			getArgv(void) const;

	// Coplien
		virtual ~ExecStream(void);
		explicit ExecStream(ExecStream const & ref);
		ExecStream const &			operator=(ExecStream const & rhs);

		explicit ExecStream(std::string const & path, std::string const & argv, std::shared_ptr<utils::Env> env = nullptr);

			/**
			 * \brief Send signal.
			 *
			 * like the kill(2) function, send a signal to the concerned process.
			*/
		void						kill(int sig = SIGTERM);

			/**
			 * \brief Exec process.
			 *
			 * Perform a fork(2), followed by a call of the fork callback (onFork() function) and an execve(2)
			*/
		void						exec(bool separate_stderr = true);


			/**
			 * \brief Fork hook.
			 * \param f is a callback : std::function<void(ExecStream *)>
			 *
			 * This function allows you to exec code in a child process before the call of execve(2)
			*/
		void						onFork(Callback f);

			/**
			 * \brief Exit hook.
			 * \param f is a callback : std::function<void(ExecStream *, uint8_t, bool)>
			 *
			 * This function allows you to watch when a process exited.
			*/
		void						onExit(Exit_Callback f);


			/**
			 * \brief Receive hook.
			 * \note see Stream::onReceive()
			*/
		void						onReceive(Recv_Callback f);

			/**
			 * \brief Error Receive hook.
			 * \note see Stream::onErrReceive()
			*/
		void						onErrReceive(Recv_Callback f);

			/**
			 * \brief Plug hook.
			 * \note see Stream::onPlug()
			*/
		void						onPlug(Callback f);

			/**
			 * \brief Unplug hook.
			 * \note see Stream::onUnplug()
			*/
		void						onUnplug(Callback f);

			/**
			 * \brief Close hook.
			 * \note see Stream::onClose()
			*/
		void						onClose(Callback f);

			/**
			 * \note see io::Stream::send(Message *, Callback)
			*/
		void						send(Message *m, Callback f = 0);

			/**
			 * \note see io::Stream::send(Message *, size_t, Callback)
			*/
		void						send(Message *m, size_t len, Callback f = 0);

			/**
			 * \note see io::Stream::send(Message *, size_t, size_t, Callback)
			*/
		void						send(Message *m, size_t start, size_t end, Callback f = 0);

			/**
			 * \note see io::Stream::send(char const * m, Callback f)
			*/
		void						send(char const * m, Callback f = 0);
	};

} /* namespace io */

#endif /* !EXECSTREAM_HPP */
