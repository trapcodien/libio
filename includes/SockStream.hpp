#ifndef SOCKSTREAM_HPP
# define SOCKSTREAM_HPP

# include <arpa/inet.h>

# include "Stream.hpp"

namespace io
{
		/**
		 * \brief Remote TCP Connection
		 *
		 * It's the same thing as io::Stream except that a new connect() function emerged. (as well as onConnect() Callback setter)
		 *
		 * \note See examples
		*/
	class SockStream : public Stream
	{
		friend class Listener;

	public :
		typedef std::function<void(SockStream *)>	Callback;

	private:
		bool						_connected;
		std::string					_host;
		int							_port;
		sockaddr_in					_sin;
		Callback					_connectCallback;

	// Clone
		virtual SockStream *		clone(void) const;

	protected:
	// Getters
		sockaddr_in const &			getSin(void) const;
		Callback const &			getConnectCallback(void) const;

	public:
		std::string const &			getHost(void) const;
		int							getPort(void) const;
	// Coplien
	//
		explicit SockStream(void);
		virtual ~SockStream(void);
		explicit SockStream(SockStream const & ref);
		SockStream const &			operator=(SockStream const & rhs);

		explicit SockStream(std::string const & host, int port);

			/**
			 * \return boolean for SockStream connection status.
			*/
		bool						isConnected(void) const;

			/**
			 * \param host : IP Address or Hostname to the remote host.
			 * \param port : remote port.
			 * \return boolean for connection status.
			*/
		bool						connect(std::string const & host, int port);

			/**
			 * \note see Stream::close(Moment)
			*/
		virtual bool				close(Moment m);

			/**
			 * \note see Stream::close()
			*/
		virtual bool				close(void);

			/**
			 * \brief Receive hook.
			 * \note see Stream::onReceive()
			*/
		void						onReceive(std::function<void(SockStream *, Message *)> f);

			/**
			 * \brief Plug hook.
			 * \note see Stream::onPlug()
			*/
		void						onPlug(Callback f);

			/**
			 * \brief Unplug hook.
			 * \note see Stream::onUnplug()
			*/
		void						onUnplug(Callback f);

			/**
			 * \brief Close hook.
			 * \note see Stream::onClose()
			*/
		void						onClose(Callback f);

			/**
			 * \brief Connect hook.
			 * \param f is a callback : std::function<void(SockStream *)>
			 *
			 * When a connection is up. this event is triggered.
			*/
		void						onConnect(Callback f);

			/**
			 * \note see io::Stream::send(Message *, Callback)
			*/
		void						send(Message *m, Callback f = 0);

			/**
			 * \note see io::Stream::send(Message *, size_t, Callback)
			*/
		void						send(Message *m, size_t len, Callback f = 0);

			/**
			 * \note see io::Stream::send(Message *, size_t, size_t, Callback)
			*/
		void						send(Message *m, size_t start, size_t end, Callback f = 0);

			/**
			 * \note see io::Stream::send(char const * m, Callback f)
			*/
		void						send(char const * m, Callback f = 0);
	};
}

#endif /* !SOCKSTREAM_HPP */
