#ifndef RAW_RECEIVER_HPP
# define RAW_RECEIVER_HPP

# include "internal/AReceiver.hpp"

namespace io
{
	class RawReceiver : public AReceiver
	{
	private :
	// Private Coplien
		explicit RawReceiver(void);

	// Private Clone
		AReceiver *				clone(void) const;
		
	public :
	// Constructor
		explicit RawReceiver(Stream * const stream, size_t const buffSize);

	// Coplien
		virtual ~RawReceiver(void);
		explicit RawReceiver(RawReceiver const & ref);
		RawReceiver const &		operator=(RawReceiver const & rhs);

	// Sync
		virtual void			sync(size_t readedBytes);
	};
}

#endif /* !RAW_RECEIVER_HPP */
