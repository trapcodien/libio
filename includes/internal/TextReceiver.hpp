#ifndef TEXT_RECEIVER_HPP
# define TEXT_RECEIVER_HPP

# include "internal/AReceiver.hpp"

namespace io
{
	class TextReceiver : public AReceiver
	{
	private :
		std::string		_separator;
		bool			_keepSeparator;

	// Private Coplien
		explicit TextReceiver(void);

	// Private Clone
		AReceiver *					clone(void) const;

	protected :
	// Getters
		std::string const & 		getSeparator(void) const;
		bool						getKeepSeparator(void) const;
		
	public :
	// Constructor
		explicit TextReceiver(Stream * const stream, size_t const buffSize, std::string const & separator = "\n", bool keepSeparator = false);

	// Coplien
		virtual ~TextReceiver(void);
		explicit TextReceiver(TextReceiver const & ref);
		TextReceiver const &		operator=(TextReceiver const & rhs);

	// Sync
		virtual void			sync(size_t readedBytes);
	};
}

#endif /* !TEXT_RECEIVER_HPP */
