#ifndef EMITTER_HPP
# define EMITTER_HPP

# include <string.h>
# include <functional>
# include <queue>

# include "stuff.hpp"
# include "Message.hpp"

namespace io
{
	class Stream;

	class Emitter
	{
	public :
		typedef std::function<void(Stream *)>	Callback;
	
	private :
		enum { DefaultBuffSize = BUFF_SIZE };

		struct Data
		{
			size_t			cursor;
			size_t			end;
			char const *	cmsg;
			Message *		msg;
			Callback		callback;
		};

		// Getters
		Stream * const			_stream;
		size_t					_buffSize;
		std::queue<Data>		_sendingQueue;

	// Private Coplien
		explicit Emitter(void);

	protected :
		Stream *					getStream(void) const;
		std::queue<Data> const &	getSendingQueue(void) const;

	public:
	// Constructor
		explicit Emitter(Stream * const stream, size_t buffSize);

	// Coplien
		virtual ~Emitter(void);
		explicit Emitter(Emitter const & ref);
		Emitter const &		operator=(Emitter const & rhs);
	
	// buffSize Setter/ Getter
		size_t						getBuffSize(void) const;
		void						setBuffSize(size_t buffSize);

	// Functions
		bool						diffuse(void);
		void						add(Message *m, size_t start, size_t end, Callback f = 0);
		void						add(char const *m, size_t start, size_t end, Callback f = 0);
	};
}

#endif /* !EMITTER_HPP */
