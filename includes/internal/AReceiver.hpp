#ifndef A_RECEIVER_HPP
# define A_RECEIVER_HPP

# include <functional>

# include "stuff.hpp"
# include "Message.hpp"

namespace io
{
	class Stream;

	class AReceiver : public Garbage
	{
	public:
		typedef std::function<void(Stream *, Message *)> Callback;

	private:
		enum { DefaultBuffSize = BUFF_SIZE };

		Stream * const	_stream;
		size_t			_buffSize;

	protected:
		Callback		_callback;
		Message			*_buffer;
		size_t			_readedBytes;

	// Getters
		Callback				getCallback(void) const;
		Message *				getBuffer(void) const;
		size_t					getReadedBytes(void) const;
		Stream *				getStream(void) const;
	
	// Setter
		void					setBuffer(Message * buffer);

	// Protected Coplien
		explicit AReceiver(void);

	public:
	// Constructor
		explicit AReceiver(Stream * const stream, size_t buffSize);

	// Coplien
		virtual ~AReceiver(void);
		explicit AReceiver(AReceiver const & ref);
		virtual AReceiver const &		operator=(AReceiver const & rhs);

	// Getter
		size_t					getBuffSize(void) const;

	// Setters
		void					setBuffSize(size_t buffSize);
		void					setCallback(Callback callback);

	// Get Read Pointer
		virtual char *			getReadPtr(void);

	// Sync
		void					sync(void);
		virtual void			sync(size_t readedBytes) = 0;

	// Public clone
		virtual AReceiver *		clone(void) const = 0;
	};
}

#endif /* !A_RECEIVER_HPP */
