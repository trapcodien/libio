# ifndef ENV_HPP
# define ENV_HPP

# include <unordered_map>

namespace utils
{
	class Env
	{
	private:
		char ** _env;
	
	public:
		typedef std::unordered_map<std::string, std::string> ParsedEnv;

		static void 	destroy(char **& env);
		static void 	copy(char **& dest, char ** const & source);
		static char **	create(ParsedEnv const & p_env);
	
		Env(char ** e);
		Env(ParsedEnv const & p_env);
		Env(void);
		~Env(void);
		Env(Env const & ref);
		Env const &		operator=(Env const & rhs);
	
		char * const *	getEnv(void);
		void			setEnv(ParsedEnv const & p_env);
		void			debug(void) const;
	};
}


#endif /* !ENV_HPP */

