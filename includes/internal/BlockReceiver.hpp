#ifndef BLOCK_RECEIVER_HPP
# define BLOCK_RECEIVER_HPP

# include "internal/AReceiver.hpp"

namespace io
{
	class BlockReceiver : public AReceiver
	{
	private :
		enum { DefaultBlockSize = 8 };
		size_t			_blockSize;

	// Private Coplien
		explicit BlockReceiver(void);

	// Private Clone
		AReceiver *					clone(void) const;
		
	public :
	// Constructor
		explicit BlockReceiver(Stream * const stream, size_t const buffSize, size_t blockSize = DefaultBlockSize);

	// Coplien
		virtual ~BlockReceiver(void);
		explicit BlockReceiver(BlockReceiver const & ref);
		BlockReceiver const &		operator=(BlockReceiver const & rhs);

		// blockSize Getter/Setter
		size_t						getBlockSize(void) const;
		void						setBlockSize(size_t blockSize);

	// Sync
		virtual void				sync(size_t readedBytes);
	};
}



#endif /* !BLOCK_RECEIVER_HPP */
