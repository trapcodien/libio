#ifndef DESTRUCTOR_HPP
# define DESTRUCTOR_HPP

# include <map>

# include "Garbage.hpp"

namespace io
{
	/**
	 * \brief Store Garbage objects and destroy them properly when necessary.
	 *
	 * \warning Never instanciate Collector, please always refer to the io::GC global.
	 *
	 * io::GC is a global Collector and used by many internal io objects \n
	 * It provides 3 basic functions :
	 * - lock (call by Garbage heap allocation)
	 * - unlock
	 * - forget (call by Garbage destructor)
	 *
	 * \note lock/unlock calls are arranged by a counter, lock increments it and unlock decrements it, when the counter reach zero, heap Garbage object is deleted.
	*/
	class Collector
	{
	private:
		typedef std::map<Garbage *, int>	GarbageCollector;

		GarbageCollector					_objects;

	protected:
	//	Getter
		GarbageCollector const &	getObjects(void) const;

	public:
	// Coplien
		explicit Collector(void);
		virtual ~Collector(void);
		explicit Collector(Collector const & ref);
		Collector const &			operator=(Collector const & rhs);

	/**
	 * \brief guarantees access to a heap garbage resource
	 *
	 * \param g : Pointer to a Garbage object.
	 * \return - the number of times the object has been locked. \n - return -1 if g is NULL or g is not a heap resource.
	 *
	 * This function is automatically called each time a dynamic allocation occurs.
	 *
	 * When an object is locked for the first time, Collector will store it in a std::map
	 * A single object can be locked multiple times.\n
	 * \note In libIO, lock is very useful, it allows you to keep volatile asynchronous data, for future uses.
	 *
	*/
		int							lock(Garbage * g);

	/**
	 * \brief Release locked garbage object.
	 *
	 * \param g : Pointer to a Garbage object.
	 * \return - the number of times the object has been unlocked. \n - return -1 if g is NULL, is not a heap resource or doesn't exits in GC.
	 *
	 * This function just decrement a counter (see Collector description).\n
	 * When the counter reach zero, heap garbage object is properly deleted.
	 *
	*/
		int							unlock(Garbage * g);

	/**
	 * \brief Forget locked garbage object
	 *
	 * \param g : Pointer to a Garbage object.
	 * \param destroy : If g must be deleted.
	 *
	 * This function allow you to forget a Garbage object.
	*/
		void						forget(Garbage * g, bool destroy = true);
	};

	/**
	 * \brief Global Collector
	 *
	 * This is the main global Collector.\n
	 * It allows you to use lock/unlock/forget io::Collector functions in all scopes :
	 * - io::Collector::lock()
	 * - io::Collector::unlock()
	 * - io::Collector::forget()
	 *
	 * \warning Never instanciate Collector, please always refer to the io::GC global.
	*/
	extern Collector				GC;
}

#endif /* !DESTRUCTOR_HPP */
