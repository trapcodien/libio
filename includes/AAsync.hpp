#ifndef A_ASYNC_HPP
# define A_ASYNC_HPP

# include "Garbage.hpp"
# include "Multiplexer.hpp"
# include "stuff.hpp"

namespace io
{
	class Multiplexer;

	/**
	 * \brief Abstract Asynchronous object
	 *
	 * AAsync objects works with Multiplexer. \n
	 * Exposes some basic functions like :
	 * - AAsync::close(Moment)
	 * - AAsync::unplug()
	 * - AAsync::getController()
	 *
	 * \note It can't be instanciated (abstract).
	 *
	*/
	class AAsync : public Garbage
	{
		friend class Multiplexer;
	private:
		bool			_read;
		bool			_write;
		int				_fdIn;
		int				_fdOut;
		int				_fdErr;

	protected:
		Multiplexer *	_controller;
		bool			_shouldClose;

	// fd Setter
		void				fdIn(int fdIn);
		void				fdOut(int fdOut);
		void				fdErr(int fdOut);

	// AAsync Methods
		virtual bool		triggerIn(void) = 0;
		virtual bool		triggerOut(void) = 0;
		virtual bool		triggerErr(void) = 0;
		virtual void		triggerPlugged(void) = 0;
		virtual void		triggerUnplugged(void) = 0;

		void				shouldClose(void);

	public:
	// read/write Getters
		bool				read(void) const;
		bool				write(void) const;

	// read/write Setters
		void				read(bool state);
		void				write(bool state);

	// Constructor
		explicit AAsync(int fdInt, int fdOut = -1);

	// Coplien
		explicit AAsync(void);
		virtual ~AAsync(void);
		explicit AAsync(AAsync const & ref);
		AAsync const &		operator=(AAsync const & rhs);

			/**
			 * \brief file descriptor getter
			 * \return AAsync file descriptor (-1 if not set)
			*/
		int					fdIn(void) const;
		int					fdOut(void) const;
		int					fdErr(void) const;

			/**
			 * \return NULL if AAsync object is not plugged.
			*/
		Multiplexer *		getController(void) const;

			/**
			 * \param Moment : useful for waiting a client to clean all his sending queue.
			 * It can be :
			 * - io::now : Shutdown brutally
			 * - io::after_send : Wait for the sending queue to be empty before closing.
			 * - io::after_next_send : Wait next send before closing.
			*/
		virtual bool		close(Moment m) = 0;

			/**
			 * If AAsync object is plugged, simply unplug it. \n
			 * \note Stream::onUnplug() is triggered if set
			*/
		void				unplug(void);
	};
}

#endif /* !A_ASYNC_HPP */
