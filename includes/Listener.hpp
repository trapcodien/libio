#ifndef LISTENER_HPP
# define LISTENER_HPP

# include "SockStream.hpp"

namespace io
{
		/**
		 * \brief Bind a port and receive new TCP connections.
		 *
		 * A Listener allows you to simply bind a port, and receive new TCP connections as a model \n
		 * When a new client connects, a SockStream is created using the SockStream::clone() method (on the model).
		 *
		 * A Listener can set a model. A model is a SockStream pointer used to allocate fresh new connections. \n
		 * You can apply callbacks on your model before setting it.
		 *
		 * \note You must plug your Listener into a Multiplexer to receive new connections.
		 * \note See examples
		 *
		*/
	class Listener : public AAsync
	{
	private :
		SockStream *		_model;
		int					_port;
		int					_backlog;
		sockaddr_in			_sin;
		bool				_listening;

	// Private close implementation
		bool					close(Moment m);

		protected :
	// Protected Getter
		sockaddr_in const &		getSin(void) const;

	// Protected  clone implementation
		virtual AAsync *		clone(void) const;

	// AAsync implementation
		bool					triggerIn(void);
		bool					triggerOut(void);
		bool					triggerErr(void);
		void					triggerPlugged(void);
		void					triggerUnplugged(void);

		public :
	// Coplien
		explicit Listener(void);
		virtual ~Listener(void);
		explicit Listener(Listener const & ref);
		Listener const &		operator=(Listener const & rhs);

	// Constructor
		explicit Listener(int port, int backlog = DEFAULT_BACKLOG);

			/**
			 * \return the model pointer (NULL if no model)
			*/
		SockStream *			getModel(void) const;

			/**
			 * \return Listening **port**
			 * \return -1 if no Listening **port**
			*/
		int						getPort(void) const;

			/**
			 * \return Backlog (see listen(2) manpage)
			*/
		int						getBacklog(void) const;

		bool					isListening(void) const;

			/**
		 	* \param model : SockStream pointer that represent a new fresh connection.
		 	*
			 * When a Listener is plugged and receive a new TCP connection, model will be cloned and automatically plugged to the Listener's controller.
			*/
		void					setModel(SockStream * model);

			/**
			 * \param port : Listening port
			 * \param backlog : maximum length for the queue of pending connections
			 *
			 * \note A Listener can be bound to only port. If you want to listen multiple ports, use multiple Listener objects
			 *
			*/
		bool					listen(int port, int backlog = DEFAULT_BACKLOG);
		void					stopListen(void);
	};
}

#endif /* !LISTENER_HPP */
