#ifndef STUFF_HPP
# define STUFF_HPP

	/**
	* io namespace is the main namespace for libIO
	*/
namespace io
{
		/**
		* \brief I/O Default Buffer Size
		*/
	enum { BUFF_SIZE = 4096 };

		/**
		* \brief I/O Default Backlog for listener.
		*/
	enum { DEFAULT_BACKLOG = 42 };

		/**
		* \brief Moment is used by io::Stream::close(Moment)
		*/
	enum Moment { now = 1, after_send = 2, after_next_send = 3 };

		/**
		* \brief io::non_block can be passed in parameter to the io::Multiplexer::ioPoll(long) function
		*/
	enum PollType { non_block = 0 };
}

#endif /* STUFF_HPP */
