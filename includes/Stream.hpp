#ifndef STREAM_HPP
# define STREAM_HPP

# include <functional>

# include "internal/RawReceiver.hpp"
# include "internal/TextReceiver.hpp"
# include "internal/BlockReceiver.hpp"
# include "internal/Emitter.hpp"

# include "AAsync.hpp"

namespace io
{
	class AReceiver;
	class Message;

		/**
		 * \brief Represents an asynchronous file descriptor I/O object.
		 *
		 * This class can be used in many situations.\n
		 * It allow you to asynchronusly read/write on a file descriptor passing by a Multiplexer.
		 *
		 * For example, you can Multiplexer::plug() a **Stream(0)** object for watching keyboards events. \n
		 * You can also lock **Stream(1)** and use send() function to bufferize stdout. \n
		*/
	class Stream :  public AAsync
	{
	public :
		enum { DefaultBuffSize = BUFF_SIZE };
		typedef std::function<void(Stream *)>		Callback;

	private :

		AReceiver *			_receiver;
		AReceiver *			_err_receiver;
		Emitter				_emitter;

	protected :
	// AAsync Overload
		virtual bool		triggerIn(void);
		virtual bool		triggerOut(void);
		virtual bool		triggerErr(void);
		virtual void		triggerPlugged(void);
		virtual void		triggerUnplugged(void);

		Callback			_plugCallback;
		Callback			_unplugCallback;
		Callback			_closeCallback;

	// Callback Getters
		Callback const &	getPlugCallback(void) const;
		Callback const &	getUnplugCallback(void) const;
		Callback const &	getCloseCallback(void) const;

	// Emitter
		Emitter const &		getEmitter(void) const;

	// Receiver Getter/Setter
		AReceiver *			receiver(void) const;
		void				receiver(AReceiver * new_receiver);
		AReceiver *			err_receiver(void) const;
		void				err_receiver(AReceiver * new_receiver);

	public :
	// Coplien
		explicit Stream(void);
		virtual ~Stream(void);
		explicit Stream(Stream const & ref);
		Stream &		operator=(Stream const & rhs);

	// Constructor
		explicit Stream(int fdIn, int fdOut = -1);

			/**
			 * \brief I/O Buffer size getter
			 * \return buffSize value
			 * \note buffSize deals with all I/O on the Stream.
			*/
		size_t				buffSize(void) const;

			/**
			 * \brief I/O Buffer size setter
			 * \param buffSize : Number of bytes readed/writed
			 * \note buffSize deals with all I/O on the Stream.
			*/
		void				buffSize(size_t buffSize); // I/O Buffer size setter

		virtual bool		close(Moment m);

			/**
			 * \brief Equivalent to close(io::after_send)
			 *
			 * This function is a shortcut for close(io::after_send).
			 * \note see close(Moment)
			*/
		virtual bool		close(void);

			/**
			 * \brief Receive hook
			 * \param f is a callback : std::function<void(Stream *, Message *)>
			 *
			 * This function allows you to set a new behavior on receive event.
			*/
		void				onReceive(std::function<void(Stream *, Message *)> f); // Receive event

			/**
			 * \brief Receive stderr hook
			 * \param f is a callback : std::function<void(Stream *, Message *)>
			 *
			 * This function allows you to set a new behavior on receive stderr (useful for io::ExecStream)
			*/
		void				onErrReceive(std::function<void(Stream *, Message *)> f); // Receive event

			/**
			 * \brief Plug hook
			 * \param f is a callback : std::function<void(Stream *)>
			 *
			 * This function allows you to watch when the Stream is plugged in a Multiplexer.
			*/
		void				onPlug(Callback f); // Plug event

			/**
			 * \brief Unplug hook
			 * \param f is a callback : std::function<void(Stream *)>
			 *
			 * This function allows you to watch when the Stream is unplugged.
			*/
		void				onUnplug(Callback f); // Unplug event

			/**
			 * \brief Close hook
			 * \param f is a callback : std::function<void(Stream *)>
			 *
			 * This function allows you to watch when the Stream is closed.
			*/
		void				onClose(Callback f); // Close event

			/**
			 * \param m : message to send
			 * \param f : Callback for knowing when the message is sent.
			*/
		void				send(Message *m, Callback f = 0);

			/**
			 * \param m : message to send.
			 * \param len : Length of the message.
			 * \param f : Callback for knowing when the message is sent.
			 *
			 * This function is useful for sending segmented message.
			 * \note see example in Stream::send()
			*/
		void				send(Message *m, size_t len, Callback f = 0);

			/**
			 * \param m : message to send.
			 * \param start : Starting offset of the message.
			 * \param end : Ending offset of the message.
			 * \param f : Callback for knowing when the message is sent.
			 *
			 * \note message size = end - start
			 *
			 * This function is useful for sending segmented message.
			 *
			 * \note see example in Stream::send()
			*/
		void				send(Message *m, size_t start, size_t end, Callback f = 0);

			/**
			 * \param m : const message
			 * \param f : Callback for knowing when the message is sent.
			 *
			 * This function is used to send const string message.
			 *
			 * \note see example in Stream::send()
			*/
		void				send(char const * m, Callback f = 0);

			/**
			 * \brief set RAW receive mode
			 *
			 * rawMode is useful for creating real Stream.
			 *
			*/
		void				rawMode(void);

			/**
			 * \brief set TEXT receive mode
			 *
			 * \param separator : used for cutting message. ('\\n' does a get_line behavior)
			 * \param keepSeparator : allows to keep the separator in the received Message.
			*/
		void				textMode(std::string const & separator = "\n", bool keepSeparator = false);

			/**
			 * \brief set BLOCK receive mode
			 *
			 * \param blockSize : size of a received block in bytes.
			 *
			 * blockMode will trigger the onReceive event when Message size is sufficient.\n
			 *
			 * \note blockMode will strictly cut your message.
			*/
		void				blockMode(size_t blockSize);
	};
}

#endif /* !STREAM_HPP */
