# ifndef TIMER_HPP
# define TIMER_HPP

# include <tuple>
# include <unordered_map>
# include <map>
# include <functional>

namespace io
{
	class Timer
	{
	public:

		/**
		 * \brief Get actual timestamp in milliseconds.
		 * \return actual timestamp (in milliseconds).
		*/
		static uint64_t			now(void);

		/**
		 * \brief Timer Callback
		 * \param uint64_t : Time Event Identifier
		*/
		typedef std::function<void(uint64_t)>										Callback;

	private:

		/**
		 * \brief Time Infos
		 * \param uint64_t : Time Event Identifier.
		 * \param Callback : Timer Callback.
		 * \param bool : true if interval.
		 * \param uint64_t : Duration.
		*/
		typedef std::tuple<uint64_t, Callback, bool, uint64_t>						TimeInfos;

		/**
		 * \brief Times structure
		 * \param uint64_t : Timestamp when trigger event. (key of the map)
		 * \param TimeInfos : A tuple with some time infos.
		*/
		typedef std::multimap<uint64_t, TimeInfos>										Times;

		/**
		 * \brief 
		 * \param uint64_t : Timestamp when trigger event. (key of the map)
		 * \param TimeInfos : A tuple with some time infos.
		*/
		typedef std::unordered_map<uint64_t, uint64_t>								LinkedTimes;


		void					triggerTimeEvent(Times::iterator & infos);
	
		// Attributes
		int						_id = 0;
		Times					_timesList;
		LinkedTimes				_linkedTimes;
	
	public:
		Timer(void);
		~Timer(void);
		Timer(Timer const & ref);
		Timer const &		operator=(Timer const & rhs);

		/**
		 * \brief Set a timer hook event.
		 * \return Time event identifier.
		 * \param duration : Time in ms.
		 * \param f : Callback
		 * \param interval : Interval mode.
		*/
		uint64_t			setHook(uint64_t duration, Callback f, bool interval = false);

		/**
		 * \brief Clear a timer hook event.
		 * \return True if clear successfully.
		 * \param id : Time Event Identifier.
		*/
		bool				clrHook(uint64_t id);

		/**
		 * \brief Trigger timer events if necessary 
		 * \return 0 if no timer event, otherwise return the time to wait (in ms).
		*/
		uint64_t			timePoll(void);
	};
}


#endif /* !TIMER_HPP */

