#ifndef MESSAGE_HPP
# define MESSAGE_HPP

# include <iostream>

# include "Collector.hpp"

namespace io
{
	/**
	 * \brief Collectable string (mix of std::string and io::Garbage)
	*/
	class Message : public std::string, public Garbage
	{
	};
}

#endif /* !MESSAGE_HPP */
