#ifndef MULTIPLEXER_HPP
# define MULTIPLEXER_HPP

# include <time.h>
# include <sys/select.h>
# include <queue>
# include <map>

# include "Exception.hpp"
# include "Collector.hpp"
# include "Timer.hpp"
# include "AAsync.hpp"

namespace io
{
	class AAsync;

		/**
		 * \brief File descriptor Manager - work with any io::AAsync objects
		 *
		 * Multiplexer does 2 things :
		 * - Plug/Unplug an AAsync object.
		 * - poll I/O Events
		 *
		 * Multiplexing Practices
		 * ----------------------
		 * Multiplexers can be used in 3 ways :
		 * - Blocking poll (Multiplexer::ioPoll())
		 * - NonBlocking poll (Multiplexer::ioPoll(long) with 0 or io::non_blocking)
		 * - Based clock time (Multiplexer::ioPoll(long) with a sleep time in milliseconds)
		*/
	class Multiplexer
	{
		friend class AAsync;
	private:
		typedef std::map<int, AAsync *>			NetworkMap;
		typedef std::queue<AAsync *>			DestroyQueue;

		NetworkMap		_entries;
		DestroyQueue	_destroyer;
		fd_set			_readFds;
		fd_set			_writeFds;
		timeval			_tv;
		Timer			_timer;

		void					clean(void);
		void					setWritter(int fd);
		void					unsetWritter(int fd);
		void					setReader(int fd);
		void					unsetReader(int fd);
		void					setError(int fd);
		void					unsetError(int fd);

		void					poll(timeval * tv);

	protected:
	// Getters
		NetworkMap const &		getEntries(void) const;
		DestroyQueue const &	getDestroyer(void) const;
		fd_set const &			getReadFds(void) const;
		fd_set const &			getWriteFds(void) const;
		timeval const &			getTv(void) const;

	public:
	// Coplien
		explicit Multiplexer(void);
		virtual ~Multiplexer(void);
		explicit Multiplexer(Multiplexer const & ref);
		Multiplexer const &		operator=(Multiplexer const & rhs);


			/**
			 * \return  true if successfully plugged.
			 * \param asyncObj : Asynchronous object pointer
			 *
			 * When the Asynchronous object is plugged, his AAsync::onPlug() Callback is called. \n
			 *
			 * \note If an AAsync object is already plugged, it will be properly unplugged before replugging it.
			 *
			 * \warning Only file descriptors between 0 and FD_SETSIZE are accepted. (see select(2) limitation)
			 *
			*/
		bool					plug(AAsync *asyncObj);

			/**
			 * \return  true if successfullly unplugged.
			 * \param asyncObj : Asynchronous object pointer
			 *
			 * This function allows you to tell Multiplexer to stop watching on an AAsync object.\n
			 * If object is not plugged, nothing is done.
			*/
		bool					unplug(AAsync *asyncObj);

			/**
			 * \return  numbers of objects that was unplugged.
			 * With this function, you can simply unplug all AAsync objects to stop your I/O loop
			*/
		int						unplugAll(void);

			/**
			 * \param asyncObj : Asynchronous object pointer
			 * \return A boolean for know if AAsync is plugged.
			*/
		bool					isPlugged(AAsync *asyncObj) const;

			/**
			 * \return Numbers of plugged AAsync objects. (can't be negative)
			*/
		int						getNbPlugged(void) const;

			/**
			 * \brief I/O loop condition
			 *
			 * This function allow you to test if Multiplexer is ready for ioPoll(). (see example)
			*/
		explicit				operator bool(void);

			/**
			 * \brief Blocking poll
			 *
			 * Purpose : trigger AAsync Events when receiving/sending data
			 *
			 * \note Call this function cause an indetermined waiting time
			*/
		void					ioPoll(void);

			/**
			 * \brief NonBlocking poll
			 * \param waitingTime : time to wait in milliseconds
			 *
			 * **Purpose** : trigger AAsync Events when receiving/sending data \n
			 *
			 * This is the NonBlocking ioPoll() implementation.
			*/
		void					ioPoll(int64_t waitingTime);

			/**
			 * \return  Time event identifier.
			 * \param duration : Time in ms.
			 * \param f : Timer::Callback is std::function<void(void)>.
			 *
			 * Set a single timed event.
			*/
		uint64_t				setTimeout(uint64_t duration, Timer::Callback f);

			/**
			 * \return  Time event identifier.
			 * \param duration : Time in ms.
			 * \param f : Timer::Callback is std::function<void(void)>.
			 *
			 * Set an interval event.
			*/
		uint64_t				setInterval(uint64_t duration, Timer::Callback f);

			/**
			 * \return  true if event is successfully cleared.
			 * \param id : Time event identifier.
			 *
			 * Clear a single timed event.
			*/
		bool					clrTimeout(uint64_t id);

			/**
			 * \return  true if event is successfully cleared.
			 * \param id : Time event identifier.
			 *
			 * Clear an interval event.
			*/
		bool					clrInterval(uint64_t id);
	};
}
#endif /* !MULTIPLEXER_HPP */
