#ifndef EXCEPTION_HPP
# define EXCEPTION_HPP

# include <iostream>

namespace io
{
	/**
	 * \brief Very basic error handling class
	*/
	class Exception
	{
	protected:
		std::string		_msg;

	// Getter
		std::string const &		getMsg(void) const;

	private:
	// Private Coplien
		explicit Exception(void);

	public:
		explicit Exception(std::string const & msg, std::string reason = "");

	// Coplien
		virtual ~Exception(void) throw();
		Exception(Exception const & ref);
		Exception const &		operator=(Exception const & rhs);

	// What Overload
		virtual const char *	what(void) const throw();
	};
}

#endif /* !EXCEPTION_HPP */
