#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <errno.h>

#include "Stream.hpp"
#include "Exception.hpp"

namespace io
{
// Coplien
	Stream::Stream(void)
	: AAsync(), _receiver(new TextReceiver(this, Stream::DefaultBuffSize)), _err_receiver(new TextReceiver(this, Stream::DefaultBuffSize)), _emitter(this, Stream::DefaultBuffSize), _plugCallback(0), _unplugCallback(0), _closeCallback(0)
	{
	}

	Stream::~Stream(void)
	{
		GC.unlock(this->_receiver);
		GC.unlock(this->_err_receiver);
	}

	Stream::Stream(Stream const & ref) : AAsync(), _emitter(ref.getEmitter())
	{
		*this = ref;
	}

	Stream &	Stream::operator=(Stream const & rhs)
	{
		this->_emitter = rhs.getEmitter();
		this->_plugCallback = rhs.getPlugCallback();
		this->_unplugCallback = rhs.getUnplugCallback();
		this->_closeCallback = rhs.getCloseCallback();
		AAsync::operator=(rhs);
		this->receiver(rhs.receiver()->clone());
		*this->receiver() = *rhs.receiver();
		this->err_receiver(rhs.err_receiver()->clone());
		*this->err_receiver() = *rhs.err_receiver();
		return (*this);
	}

// Constructor
	Stream::Stream(int fdIn, int fdOut)
	: AAsync(fdIn, fdOut), _receiver(new TextReceiver(this, Stream::DefaultBuffSize)), _err_receiver(new TextReceiver(this, Stream::DefaultBuffSize)), _emitter(this, Stream::DefaultBuffSize), _plugCallback(0), _unplugCallback(0), _closeCallback(0)
	{
	}

// Callback Getters
	Stream::Callback const &	Stream::getPlugCallback(void) const
	{
		return this->_plugCallback;
	}

	Stream::Callback const &	Stream::getUnplugCallback(void) const
	{
		return this->_unplugCallback;
	}

	Stream::Callback const &	Stream::getCloseCallback(void) const
	{
		return this->_closeCallback;
	}

	Emitter const &				Stream::getEmitter(void) const
	{
		return this->_emitter;
	}

// Receiver Getter/Setter
	AReceiver *				Stream::receiver(void) const
	{
		return this->_receiver;
	}

	void					Stream::receiver(AReceiver * new_receiver)
	{
		if (!new_receiver)
			return ;
		if (this->_receiver)
		{
			*new_receiver = *this->_receiver;
			GC.unlock(this->_receiver);
		}
		this->_receiver = new_receiver;
		this->_receiver->sync();
	}

	AReceiver *				Stream::err_receiver(void) const
	{
		return this->_err_receiver;
	}

	void					Stream::err_receiver(AReceiver * new_receiver)
	{
		if (!new_receiver)
			return ;
		if (this->_err_receiver)
		{
			*new_receiver = *this->_err_receiver;
			GC.unlock(this->_err_receiver);
		}
		this->_err_receiver = new_receiver;
		this->_err_receiver->sync();
	}

// buffSize Getter/Setter
	size_t					Stream::buffSize(void) const
	{
		return this->_emitter.getBuffSize();
	}

	void					Stream::buffSize(size_t buffSize)
	{
		this->_emitter.setBuffSize(buffSize);
		this->_receiver->setBuffSize(buffSize);
		this->_err_receiver->setBuffSize(buffSize);
	}

// Functions
	bool					Stream::close(Moment m)
	{
		if (m == after_send)
		{
			this->read(false);
			this->shouldClose();
		}
		else if (m == after_next_send)
		{
			this->shouldClose();
			this->read(false);
		}
		else
		{
			if (this->_controller)
				this->_controller->unplug(this);
			if (this->fdIn() == -1 && this->fdOut() == -1 && this->fdErr() == -1)
				return false;
			if (this->fdIn() == this->fdOut())
				this->fdOut(-1);
			if (this->fdIn() != -1 && ::close(this->fdIn()) == -1)
				throw Exception("io::Stream -> close() error (input) : ", strerror(errno));
			this->fdIn(-1);
			if (this->fdOut() != -1 && ::close(this->fdOut()) == -1)
				throw Exception("io::Stream -> close() error (output) : ", strerror(errno));
			this->fdOut(-1);
			if (this->fdErr() != -1 && ::close(this->fdErr()) == -1)
				throw Exception("io::Stream -> close() error (error) : ", strerror(errno));
			this->fdErr(-1);
			if (this->_closeCallback)
				this->_closeCallback(this);
		}
		return true;
	}

	bool					Stream::close(void)
	{
		return this->close(io::after_send);
	}

	bool					Stream::triggerIn(void)
	{
		ssize_t ret;

		ret = ::read(this->fdIn(), this->_receiver->getReadPtr(), this->_receiver->getBuffSize());
		if (ret <= 0)
			return false;

		AReceiver * receiver = this->_receiver;
		GC.lock(receiver);
		this->_receiver->sync(static_cast<size_t>(ret));
		GC.unlock(receiver);
		return true;
	}

	bool					Stream::triggerOut(void)
	{
		if (!this->_emitter.diffuse())
			this->write(false);
		if (this->_shouldClose && !this->read() && !this->write())
			this->close(io::now);
		return true;
	}

	bool					Stream::triggerErr(void)
	{
		ssize_t ret;

		if (this->fdErr() == -1)
			return false;
		ret = ::read(this->fdErr(), this->_err_receiver->getReadPtr(), this->_err_receiver->getBuffSize());
		if (ret < 0)
			return false;

		AReceiver * receiver = this->_err_receiver;
		GC.lock(receiver);
		this->_err_receiver->sync(static_cast<size_t>(ret));
		GC.unlock(receiver);
		return true;
	}


	void					Stream::triggerPlugged(void)
	{
		if (this->_plugCallback)
			this->_plugCallback(this);
	}
	
	void					Stream::triggerUnplugged(void)
	{
		if (this->_unplugCallback)
			this->_unplugCallback(this);
	}

// Callback Wrappers
	void					Stream::onReceive(std::function<void(Stream *, Message *)> f)
	{
		this->receiver()->setCallback(f);
	}

	void					Stream::onPlug(Stream::Callback f)
	{
		this->_plugCallback = f;
	}

	void					Stream::onUnplug(Stream::Callback f)
	{
		this->_unplugCallback = f;
	}

	void					Stream::onClose(Stream::Callback f)
	{
		this->_closeCallback = f;
	}

// Send
	void					Stream::send(Message * m, Stream::Callback f)
	{
		this->write(true);
		this->_emitter.add(m, 0, m->size(), f);
	}

	void					Stream::send(Message * m, size_t len, Stream::Callback f)
	{
		this->write(true);
		this->_emitter.add(m, 0, len, f);
	}

	void					Stream::send(Message *m, size_t start, size_t end, Stream::Callback f)
	{
		this->write(true);
		this->_emitter.add(m, start, end, f);
	}

	void					Stream::send(char const * m, Callback f)
	{
		this->write(true);
		this->_emitter.add(m, 0, strlen(m), f);
	}

// Receive Mode
	void					Stream::rawMode(void)
	{
		this->receiver(new RawReceiver(this, this->receiver()->getBuffSize()));
		this->err_receiver(new RawReceiver(this, this->err_receiver()->getBuffSize()));
	}

	void					Stream::textMode(std::string const & separator, bool keepSeparator)
	{
		this->receiver(new TextReceiver(this, this->receiver()->getBuffSize(), separator, keepSeparator));
		this->err_receiver(new TextReceiver(this, this->err_receiver()->getBuffSize(), separator, keepSeparator));
	}

	void					Stream::blockMode(size_t blockSize)
	{
		this->receiver(new BlockReceiver(this, this->receiver()->getBuffSize(), blockSize));
		this->err_receiver(new BlockReceiver(this, this->err_receiver()->getBuffSize(), blockSize));
	}
}
