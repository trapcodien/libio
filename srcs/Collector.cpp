#include <stack>
#include <iostream>

#include "Collector.hpp"

namespace io
{
// Coplien
	Collector::Collector(void) {  }
	Collector::~Collector(void)
	{
		while (!this->_objects.empty())
		{
			Garbage * g = this->_objects.begin()->first;
			this->_objects.erase(this->_objects.begin());
			delete g;
		}
	}

	Collector::Collector(Collector const & ref)
	{
		*this = ref;
	}

	Collector const &						Collector::operator=(Collector const & rhs)
	{
		this->_objects = rhs.getObjects();
		return *this;
	}

// Getters
	Collector::GarbageCollector const &	Collector::getObjects(void) const
	{
		return this->_objects;
	}

// Functions
	int										Collector::lock(Garbage * g)
	{
		if (!g || g->isOnStack())
			return -1;
		if (this->_objects.find(g) == this->_objects.end())
			this->_objects[g] = 0;
		return (++this->_objects[g]);
	}


	int										Collector::unlock(Garbage *g)
	{
		if (!g || g->isOnStack())
			return -1;
		GarbageCollector::iterator it = this->_objects.find(g);
		if (it == this->_objects.end())
			return -1;
		int ret = --it->second;
		if (it->second <= 0)
		{
			this->_objects.erase(it);
			delete g;
		}
		return ret;
	}


	void									Collector::forget(Garbage * g, bool destroy)
	{
		if (g->isOnStack())
			return ;
		GarbageCollector::iterator it = this->_objects.find(g);
		if (it == this->_objects.end())
			return ;
		this->_objects.erase(it);
		if (destroy)
			delete g;
	}


// Global Garbage Collector
	Collector		GC;
}
