#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <vector>
#include <sstream>
#include "ExecStream.hpp"

namespace io
{
// Private Static Utilities
	char *	ExecStream::create_path(std::string const & path)
	{
		char *ret;

		if (path.empty())
			throw (Exception("io::ExecStream : Path is empty."));
		ret = new char[path.size() + 1]();
		strcpy(ret, path.c_str());
		return ret;
	}

	char **	ExecStream::create_argv(std::string const & argv)
	{
		std::stringstream 			ss;
		std::vector<std::string> 	v;
		std::string					str;
		char						**ret;
		int							i = 0;
		
		ss << argv;
		while (ss >> str)
			v.push_back(str);
		ret = new char*[v.size() + 1]();
		for (auto it = v.begin(); it != v.end(); it++)
		{
			ret[i] = new char[it->size() + 1]();
			strcpy(ret[i], it->c_str());
			i++;
		}
		return ret;
	}

// Private Utilities
	void					ExecStream::triggerUnplugged(void)
	{
		Stream::triggerUnplugged();
		this->checkIfExited();
	}

	void					ExecStream::checkIfExited(void)
	{
		int		status;
		bool	signaled = false;
		int		exitcode = 0;
		
		if (this->_pid == -1)
			return ;
		if ((waitpid(this->_pid, &status, WNOHANG)) == -1)
			throw (Exception("ExecStream::checkIfExited : waitpid error"));
		if (WIFEXITED(status))
		{
			this->_pid = -1;
			exitcode = WEXITSTATUS(status);
		}
		else if (WIFSIGNALED(status))
		{
			this->_pid = -1;
			signaled = true;
		}
		if (this->_pid == -1 && this->_exitCallback)
			this->_exitCallback(this, exitcode, signaled);
	}

// Private Coplien
	ExecStream::ExecStream(void) : Stream(-1), _path(nullptr), _argv(nullptr) { }

// Getters
	pid_t					ExecStream::getPid(void) const { return this->_pid; }
	std::string const &		ExecStream::getPath(void) const { return this->_stringPath; }
	std::string const &		ExecStream::getArgv(void) const { return this->_stringArgv; }

// Coplien
	ExecStream::~ExecStream(void)
	{
		this->close(io::now);
		if (this->_path)
			delete [] this->_path;
		utils::Env::destroy(this->_argv);
		if (this->_pid != -1)
			::kill(this->_pid, SIGKILL);
	}
	ExecStream::ExecStream(ExecStream const & ref) : Stream(ref)
	{
		*this = ref;
	}

	ExecStream const & 		ExecStream::operator=(ExecStream const & rhs)
	{
		this->_pid = rhs._pid;
		this->_forkCallback = rhs._forkCallback;
		this->_exitCallback = rhs._exitCallback;
		*(const_cast<std::string *>(&this->_stringPath)) = rhs._stringPath;
		*(const_cast<std::string *>(&this->_stringArgv)) = rhs._stringArgv;
		if (this->_path)
			delete[] this->_path;
		this->_path = new char[strlen(rhs._path)];
		strcpy(this->_path, rhs._path);
		utils::Env::copy(this->_argv, rhs._argv);
		this->_env = rhs._env;
		Stream::operator=(rhs);
		return *this;
	}

// Constructor
	ExecStream::ExecStream(std::string const & path, std::string const & argv, std::shared_ptr<utils::Env> env)
		: Stream(-1), _pid(-1), _stringPath(path), _stringArgv(argv), _env(env)
	{
		this->_path = ExecStream::create_path(path);
		this->_argv = ExecStream::create_argv(argv);
		this->rawMode();
	}

	void						ExecStream::kill(int sig)
	{
		if (this->_pid == -1)
			return ;
		if ((::kill(this->_pid, sig)) == -1)
			throw (Exception("io::ExecStream : kill error"));
		this->checkIfExited();
	}

	void						ExecStream::exec(bool separate_stderr)
	{
		int			pipe_in[2];
		int			pipe_out[2];
		int			pipe_err[2];

		if (this->_pid != -1)
			return ;
		this->read(true);
		this->write(false);
		if (pipe(pipe_in) == -1)
			throw (Exception("io::ExecStream : pipe error"));
		if (pipe(pipe_out) == -1)
			throw (Exception("io::ExecStream : pipe error"));
		if (pipe(pipe_err) == -1)
			throw (Exception("io::ExecStream : pipe error"));
		if ((this->_pid = fork()) == -1)
			throw (Exception("io::ExecStream : fork error"));
		if (!this->_pid)
		{
			::close(pipe_in[0]);
			::close(pipe_out[1]);
			::close(pipe_err[0]);
			dup2(pipe_in[1], 1);
			dup2(pipe_out[0], 0);
			if (separate_stderr)
				dup2(pipe_err[1], 2);
			else
				dup2(pipe_in[1], 2);
			this->fdIn(pipe_in[1]);
			this->fdOut(pipe_out[0]);
			this->fdErr(pipe_err[1]);
			if (this->_forkCallback)
				this->_forkCallback(this);
			if (this->_env)
				execve(this->_path, this->_argv, this->_env->getEnv());
			else
				execve(this->_path, this->_argv, NULL);
			exit(1);
		}
		::close(pipe_in[1]);
		::close(pipe_out[0]);
		::close(pipe_err[1]);
		this->fdIn(pipe_in[0]);
		this->fdOut(pipe_out[1]);
		this->fdErr(pipe_err[0]);
	}

	void						ExecStream::onFork(ExecStream::Callback f)
	{
		this->_forkCallback = f;
	}

	void						ExecStream::onExit(ExecStream::Exit_Callback f)
	{
		this->_exitCallback = f;
	}

	void						ExecStream::onReceive(std::function<void(ExecStream *, Message *)> f)
	{
		this->receiver()->setCallback(*(reinterpret_cast<AReceiver::Callback * >(&f)));
	}

	void						ExecStream::onErrReceive(std::function<void(ExecStream *, Message *)> f)
	{
		this->err_receiver()->setCallback(*(reinterpret_cast<AReceiver::Callback * >(&f)));
	}

	void						ExecStream::onPlug(ExecStream::Callback f)
	{
		this->_plugCallback = (*(reinterpret_cast<Stream::Callback * >(&f)));
	}

	void						ExecStream::onUnplug(ExecStream::Callback f)
	{
		this->_unplugCallback = (*(reinterpret_cast<Stream::Callback * >(&f)));
	}

	void						ExecStream::onClose(ExecStream::Callback f)
	{
		this->_closeCallback = (*(reinterpret_cast<Stream::Callback * >(&f)));
	}

	void						ExecStream::send(Message *m, ExecStream::Callback f)
	{
		Stream::send(m, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}

	void						ExecStream::send(Message *m, size_t len, ExecStream::Callback f)
	{
		Stream::send(m, len, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}

	void						ExecStream::send(Message *m, size_t start, size_t end, ExecStream::Callback f)
	{
		Stream::send(m, start, end, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}

	void						ExecStream::send(char const * m, ExecStream::Callback f)
	{
		Stream::send(m, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}

} /* namespace io */

