#include <strings.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "Listener.hpp"

namespace io
{
// Private implementation
	AAsync *				Listener::clone(void) const
	{
		return new Listener(*this);
	}

	bool					Listener::close(Moment m)
	{
		this->_listening = false;
		if (this->_controller)
			this->_controller->unplug(this);
		if (this->fdIn() == -1 && this->fdOut() == -1)
			return false;
		if (this->fdIn() == this->fdOut())
			this->fdOut(-1);
		if (this->fdIn() != -1 && ::close(this->fdIn()) == -1)
			throw Exception("io::Listener -> close() error (input) : ", strerror(errno));
		this->fdIn(-1);
		if (this->fdOut() != -1 && ::close(this->fdOut()) == -1)
			throw Exception("io::Listener -> close() error (output) : ", strerror(errno));
		this->fdOut(-1);
		static_cast<void>(m);
		return true;
	}

// Protected Getter
	sockaddr_in const &		Listener::getSin(void) const
	{
		return this->_sin;
	}

// Coplien
	Listener::Listener(void)
	: AAsync(), _model(0), _port(-1), _backlog(DEFAULT_BACKLOG), _listening(false)
	{
		bzero(&this->_sin, sizeof(sockaddr_in));
	}

	Listener::~Listener(void)
	{
		this->stopListen();
		if (this->_model)
			GC.unlock(this->_model);
	}

	Listener::Listener(Listener const & ref) : AAsync(ref)
	{
		*this = ref;
	}

	Listener const &			Listener::operator=(Listener const & rhs)
	{
		this->setModel(rhs.getModel());
		this->_port = rhs.getPort();
		this->_backlog = rhs.getBacklog();
		this->_sin = rhs.getSin();
		this->_listening = rhs.isListening();
		return *this;
	}

// Constructor
	Listener::Listener(int port, int backlog)
	: AAsync(), _model(0), _port(-1), _backlog(backlog), _listening(false)
	{
		bzero(&this->_sin, sizeof(sockaddr_in));
		this->listen(port, backlog);
	}

// Getters
	SockStream *				Listener::getModel(void) const
	{
		return this->_model;
	}

	int							Listener::getPort(void) const
	{
		return this->_port;
	}

	int							Listener::getBacklog(void) const
	{
		return this->_backlog;
	}

	bool						Listener::isListening(void) const
	{
		return this->_listening;
	}

// Model Setter
	void						Listener::setModel(SockStream * model)
	{
		if (this->_model)
			GC.unlock(model);
		this->_model = model;
	}

// Functions
	bool						Listener::listen(int port, int backlog)
	{
		Multiplexer *			controller = this->getController();
		static int				one;

		this->stopListen();
		one = 1;
		this->fdIn(socket(PF_INET, SOCK_STREAM, 0));
		if (this->fdIn() == -1)
			return false;
		this->fdOut(this->fdIn());
		this->_sin.sin_family = AF_INET;
		this->_sin.sin_port = htons(port);
		this->_sin.sin_addr.s_addr = htonl(INADDR_ANY);
		if ((setsockopt(this->fdIn(), SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one))) == -1)
			throw Exception("io::Listener -> setsockopt() error : ", strerror(errno));
		if ((bind(this->fdIn(), (sockaddr *)(&this->_sin), sizeof(sockaddr_in))) == -1)
			throw Exception("io::Listener -> bind() error : ", strerror(errno));
		if ((::listen(this->fdIn(), backlog)) == -1)
			throw Exception("io::Listener -> listen() error : ", strerror(errno));
		this->_port = port;
		this->_backlog = backlog;
		if (controller)
			controller->plug(this);
		this->read(true);
		this->_listening = true;
		return true;
	}

	void						Listener::stopListen(void)
	{
		this->close(io::now);
	}

	bool						Listener::triggerIn(void)
	{
		SockStream		*s;
		socklen_t		sinsize = sizeof(sockaddr_in);
		int				fd;

		if (!this->_model)
			return false;
		s = this->_model->clone();
		if ((fd = accept(this->fdIn(), reinterpret_cast<sockaddr *>(&s->_sin), &sinsize)) == -1)
			throw Exception("io::Listener -> accept() error : ", strerror(errno));
		s->fdIn(fd);
		s->fdOut(fd);
		s->_host = inet_ntoa(s->_sin.sin_addr);
		s->_port = ntohs(s->_sin.sin_port);
		if (s->_connectCallback)
			s->_connectCallback(s);
		if (this->_controller)
			this->_controller->plug(s);
		GC.unlock(s);
		return true;
	}

	bool						Listener::triggerOut(void)
	{
		return false;
	}

	bool						Listener::triggerErr(void)
	{
		return false;
	}

	void						Listener::triggerPlugged(void)

	{
	}

	void						Listener::triggerUnplugged(void)
	{
	}

}

