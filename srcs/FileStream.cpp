#include <stdio.h>
#include "FileStream.hpp"

namespace io
{
// Private Coplien
	FileStream::FileStream(void) : Stream(-1) { }

// Getters
	std::string const &		FileStream::getFileName(void) const { return this->_fileName; }

// Coplien
	FileStream::~FileStream(void) { this->close(io::now); }
	FileStream::FileStream(FileStream const & ref) : Stream(ref), _fileName(ref.getFileName())
	{
		*this = ref;
	}

	FileStream const & 		FileStream::operator=(FileStream const & rhs)
	{
		this->_fileName = rhs._fileName;
		Stream::operator=(rhs);
		return *this;
	}

// Constructor
	FileStream::FileStream(std::string const & fileName, FileStream::Mode m) : Stream(-1), _fileName(fileName)
	{
		int		fd;

		if ((fd = open(fileName.c_str(), m)) == -1)
			throw(Exception(strerror(errno)));
		if (m == FileStream::READ)
			this->fdIn(fd);
		else
			this->fdOut(fd);
	}

	void						FileStream::onReceive(std::function<void(FileStream *, Message *)> f)
	{
		this->receiver()->setCallback(*(reinterpret_cast<AReceiver::Callback * >(&f)));
	}

	void						FileStream::onPlug(FileStream::Callback f)
	{
		this->_plugCallback = (*(reinterpret_cast<Stream::Callback * >(&f)));
	}

	void						FileStream::onUnplug(FileStream::Callback f)
	{
		this->_unplugCallback = (*(reinterpret_cast<Stream::Callback * >(&f)));
	}

	void						FileStream::onClose(FileStream::Callback f)
	{
		this->_closeCallback = (*(reinterpret_cast<Stream::Callback * >(&f)));
	}

	void						FileStream::send(Message *m, FileStream::Callback f)
	{
		Stream::send(m, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}

	void						FileStream::send(Message *m, size_t len, FileStream::Callback f)
	{
		Stream::send(m, len, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}

	void						FileStream::send(Message *m, size_t start, size_t end, FileStream::Callback f)
	{
		Stream::send(m, start, end, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}

	void						FileStream::send(char const * m, FileStream::Callback f)
	{
		Stream::send(m, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}

} /* namespace io */

