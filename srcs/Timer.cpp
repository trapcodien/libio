#include <sys/time.h>
#include "Timer.hpp"

namespace io
{
// Static
	uint64_t				Timer::now(void)
	{
		struct timeval tv;
	
		gettimeofday(&tv, nullptr);
		return ((static_cast<uint64_t>(tv.tv_sec) * 1000) + (static_cast<uint64_t>(tv.tv_usec) / 1000));
	}

// Coplien
	Timer::Timer(void) {  }
	Timer::~Timer(void) { } 
	Timer::Timer(Timer const & ref) { *this = ref; }
	Timer const & 		Timer::operator=(Timer const & rhs)
	{
		this->_timesList = rhs._timesList;
		this->_linkedTimes = rhs._linkedTimes;
		return *this;
	}

// Private
	void					Timer::triggerTimeEvent(Timer::Times::iterator & infos)
	{
		uint64_t moment = infos->first;
		uint64_t const id = std::get<0>(infos->second);
		Callback const f = std::get<1>(infos->second);
		bool const interval = std::get<2>(infos->second);
		uint64_t const duration = std::get<3>(infos->second);

		if (f) f(id);

		this->_timesList.erase(infos);
		auto it = this->_linkedTimes.find(id);
		if (it != this->_linkedTimes.end())
			this->_linkedTimes.erase(it);

		if (interval)
		{
			moment += duration;
			this->_timesList.insert({moment, std::make_tuple(id, f, interval, duration)});
			this->_linkedTimes[id] = moment;
		}
	}

	uint64_t			Timer::setHook(uint64_t duration, Callback f, bool interval)
	{
		uint64_t		moment = Timer::now() + duration;
		this->_timesList.insert({moment, std::make_tuple(this->_id, f, interval, duration)});
		this->_linkedTimes[this->_id] = moment;
		return (this->_id++);
	}

	bool				Timer::clrHook(uint64_t id)
	{
		auto linked = this->_linkedTimes.find(id);
		if (linked != this->_linkedTimes.end())
		{
			auto timeInfos = this->_timesList.find(linked->second);
			this->_linkedTimes.erase(linked);
			if (timeInfos != this->_timesList.end())
			{
				this->_timesList.erase(timeInfos);
				return true;
			}
			return false;	
		}
		return false;
	}


	uint64_t			Timer::timePoll(void)
	{
		uint64_t const	now = Timer::now();

		for (auto it = this->_timesList.begin(); it != this->_timesList.end(); it++)
		{
			if (now >= it->first)
			{
				this->triggerTimeEvent(it);
				return this->timePoll();
			}
			else
				return (it->first - now);
		}
		return 0;	
	}
} /* namespace io */


