#include "AAsync.hpp"

namespace io
{
// Coplien
	AAsync::AAsync(void)
	: _read(true), _write(false), _fdIn(-1), _fdOut(-1), _fdErr(-1), _controller(0), _shouldClose(false)
	{
	}

	AAsync::AAsync(int fdIn, int fdOut)
	: _read(true), _write(false), _fdIn(fdIn), _fdOut(fdOut), _fdErr(-1),  _controller(0), _shouldClose(false)
	{
		if (fdOut == -1)
			this->_fdOut = fdIn;
	}

	AAsync::~AAsync(void) {  }
	
	AAsync::AAsync(AAsync const & ref)
	{
		*this = ref;
	}
	
	AAsync const &		AAsync::operator=(AAsync const & rhs)
	{
		this->_fdIn = rhs.fdIn();
		this->_fdOut = rhs.fdOut();
		this->_fdErr = rhs.fdErr();
		this->_read = rhs.read();
		this->_write = rhs.write();
		this->_controller = rhs.getController();
		return *this;
	}
	
// Getters
	bool				AAsync::read(void) const { return this->_read; }
	bool				AAsync::write(void) const { return this->_write; }
	int					AAsync::fdIn(void) const { return this->_fdIn; }
	int					AAsync::fdOut(void) const { return this->_fdOut; }
	int					AAsync::fdErr(void) const { return this->_fdErr; }
	Multiplexer *		AAsync::getController(void) const { return this->_controller; }
	
// Setters
	void				AAsync::read(bool state)
	{
		if (this->_controller && state)
			this->_controller->setReader(this->fdIn());
		else if (this->_controller && !state)
			this->_controller->unsetReader(this->fdIn());
		this->_read = state;
	}

	void				AAsync::write(bool state)
	{
		if (this->_controller && state)
			this->_controller->setWritter(this->fdOut());
		else if (this->_controller && !state)
			this->_controller->unsetWritter(this->fdOut());
		this->_write = state;
	}

	void				AAsync::fdIn(int fdIn) { this->_fdIn = fdIn; }
	void				AAsync::fdOut(int fdOut) { this->_fdOut = fdOut; }
	void				AAsync::fdErr(int fdErr) { this->_fdErr = fdErr; }

	void				AAsync::shouldClose(void)
	{
		this->_shouldClose = true;
		if (!this->read() && !this->write())
			this->close(io::now);
	}

	void				AAsync::unplug(void)
	{
		if (this->_controller)
			this->_controller->unplug(this);
	}
}
