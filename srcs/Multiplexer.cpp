#include <errno.h>
#include <strings.h>
#include <stack>

#include "io.hpp"
#include "Multiplexer.hpp"

namespace io
{
// Private function clean
	void					Multiplexer::clean(void)
	{
		while (!this->_destroyer.empty())
		{
			GC.unlock(this->_destroyer.front());
			this->_destroyer.pop();
		}
	}

// Coplien
	Multiplexer::Multiplexer(void)
	{
		FD_ZERO(&this->_readFds);
		FD_ZERO(&this->_writeFds);
	}

	Multiplexer::~Multiplexer(void)
	{
		this->unplugAll();
		this->clean();
		FD_ZERO(&_readFds);
		FD_ZERO(&_writeFds);
	}

	Multiplexer::Multiplexer(Multiplexer const & ref)
	{
		*this = ref;
	}

	Multiplexer const &		Multiplexer::operator=(Multiplexer const & rhs)
	{
		this->_entries = rhs.getEntries();
		this->_destroyer = rhs.getDestroyer();
		this->_readFds = rhs.getReadFds();
		this->_writeFds = rhs.getWriteFds();
		this->_tv = rhs.getTv();
		this->_timer = rhs._timer;
		return *this;
	}

// Private Poll
	void								Multiplexer::poll(timeval * tv)
	{
		static fd_set					rfds;
		static fd_set					wfds;
		static std::queue<AAsync *>		pollInQueue;
		static std::queue<AAsync *>		pollOutQueue;
		static std::queue<AAsync *>		pollErrQueue;
		NetworkMap::iterator			it;
		int								n;
		int								maxFd;

		if (this->_entries.empty())
			return ;
		maxFd = this->_entries.rbegin()->first + 1;
		wfds = this->_writeFds;
		rfds = this->_readFds;
		n = select(maxFd, &rfds, &wfds, 0, tv);
		if (n == -1)
			throw Exception("io::Multiplexer -> select() error : ", strerror(errno));

		it = this->_entries.begin();
		while (n && it != this->_entries.end())
		{
			if (n && FD_ISSET(it->first, &wfds))
			{
				pollOutQueue.push(it->second);
				n--;
			}
			if (n && FD_ISSET(it->first, &rfds))
			{
				if (it->first == it->second->fdErr())
					pollErrQueue.push(it->second);
				else
					pollInQueue.push(it->second);
				n--;
			}
			++it;
		}

		while (!pollOutQueue.empty())
		{
			if (!pollOutQueue.front()->triggerOut())
				pollOutQueue.front()->close(io::now);
			pollOutQueue.pop();
		}

		while (!pollInQueue.empty())
		{
			if (!pollInQueue.front()->triggerIn())
				pollInQueue.front()->close(io::now);
			pollInQueue.pop();
		}

		while (!pollErrQueue.empty())
		{
			if (!pollErrQueue.front()->triggerErr())
				pollErrQueue.front()->close(io::now);
			pollErrQueue.pop();
		}

	}

// Getters
	Multiplexer::NetworkMap const &		Multiplexer::getEntries(void) const
	{
		return this->_entries;
	}

	Multiplexer::DestroyQueue const &	Multiplexer::getDestroyer(void) const
	{
		return this->_destroyer;
	}

	fd_set const &						Multiplexer::getReadFds(void) const
	{
		return this->_readFds;
	}

	fd_set const &						Multiplexer::getWriteFds(void) const
	{
		return this->_writeFds;
	}

	timeval const &						Multiplexer::getTv(void) const
	{
		return this->_tv;
	}

	bool								Multiplexer::isPlugged(AAsync *asyncObj) const
	{
		if (!asyncObj || (asyncObj->fdIn() < 0 && asyncObj->fdOut() < 0 && asyncObj->fdErr() < 0))
			return false;
		if (this->_entries.find(asyncObj->fdIn()) != this->_entries.end())
			return true;
		if (this->_entries.find(asyncObj->fdOut()) != this->_entries.end())
			return true;
		if (this->_entries.find(asyncObj->fdErr()) != this->_entries.end())
			return true;
		return false;
	}

	int									Multiplexer::getNbPlugged(void) const
	{
		return this->_entries.size();
	}

// Multiplexing functions	

	bool								Multiplexer::plug(AAsync *asyncObj)
	{
		if (!this->isPlugged(asyncObj) && asyncObj->fdIn() < FD_SETSIZE && asyncObj->fdOut() < FD_SETSIZE && asyncObj->fdErr() < FD_SETSIZE)
		{
			GC.lock(asyncObj);
			if (asyncObj->_controller)
				asyncObj->_controller->unplug(asyncObj);
			else
				this->unplug(asyncObj);
			asyncObj->_controller = this;

			if (asyncObj->fdIn() != -1)
			{
				this->_entries[asyncObj->fdIn()] = asyncObj;
				if (asyncObj->_read)
					FD_SET(asyncObj->fdIn(), &this->_readFds);
			}
			if (asyncObj->fdOut() != -1 && asyncObj->fdOut() != asyncObj->fdIn())
			{
				this->_entries[asyncObj->fdOut()] = asyncObj;
				if (asyncObj->_write)
					FD_SET(asyncObj->fdOut(), &this->_writeFds);
			}
			if (asyncObj->fdErr() != -1)
			{
				this->_entries[asyncObj->fdErr()] = asyncObj;
				FD_SET(asyncObj->fdErr(), &this->_readFds);
			}

			asyncObj->triggerPlugged();
			return true;
		}
		return false;
	}

	bool								Multiplexer::unplug(AAsync *asyncObj)
	{
		if (this->isPlugged(asyncObj))
		{
			asyncObj->_controller = 0;
			asyncObj->triggerUnplugged();
			if (asyncObj->_read)
				this->unsetReader(asyncObj->fdIn());
			if (asyncObj->_write)
				this->unsetWritter(asyncObj->fdOut());
			this->unsetError(asyncObj->fdErr());

			this->_entries.erase(asyncObj->fdIn());
			this->_entries.erase(asyncObj->fdOut());
			this->_entries.erase(asyncObj->fdErr());

			this->_destroyer.push(asyncObj);
			return true;
		}
		return false;
	}

	int									Multiplexer::unplugAll(void)
	{
		static std::stack<AAsync *>			unplugStack;
		Multiplexer::NetworkMap::iterator	it;
		int									nbUnplugged = 0;

		for (it = this->_entries.begin() ; it != this->_entries.end() ; ++it)
			unplugStack.push(it->second);
		while (!unplugStack.empty())
		{
			this->unplug(unplugStack.top());
			unplugStack.pop();
			nbUnplugged++;
		}
		return nbUnplugged;
	}

	Multiplexer::operator bool(void)
	{
		this->clean();
		if (this->_entries.empty())
			return false;
		return true;
	}

	void								Multiplexer::ioPoll(void)
	{
		uint64_t		polledTime = this->_timer.timePoll();

		if (polledTime > 0)
		{
			this->_tv.tv_sec = polledTime / 1000;
			this->_tv.tv_usec = (polledTime % 1000) * 1000;
			this->poll(&this->_tv);
		}
		else
			this->poll(nullptr);
	}

	void								Multiplexer::ioPoll(int64_t waitingTime)
	{
		uint64_t polledTime = this->_timer.timePoll();

		if (waitingTime <= 0)
		{
			bzero(&this->_tv, sizeof(timeval));
			return ;
		}

		if (polledTime > 0 && polledTime < static_cast<uint64_t>(waitingTime))
		{
			this->_tv.tv_sec = polledTime / 1000;
			this->_tv.tv_usec = (polledTime % 1000) * 1000;
			this->poll(&this->_tv);
			return ;
		}

		while (waitingTime > 0)
		{
			uint64_t start = io::Timer::now();
			this->_tv.tv_sec = waitingTime / 1000;
			this->_tv.tv_usec = (waitingTime % 1000) * 1000;
			this->poll(&this->_tv);
			waitingTime -= io::Timer::now() - start;
		}
	}

	void								Multiplexer::setWritter(int fd)
	{
		Multiplexer::NetworkMap::iterator it;
		it = this->_entries.find(fd);
		if (it != this->_entries.end())
		{
			if (!it->second->_write)
				FD_SET(fd, &this->_writeFds);
			it->second->_write = true;
		}
	}

	void								Multiplexer::unsetWritter(int fd)
	{
		Multiplexer::NetworkMap::iterator it;
		it = this->_entries.find(fd);
		if (it != this->_entries.end())
		{
			if (it->second->_write)
				FD_CLR(fd, &this->_writeFds);
			it->second->_write = false;
		}
	}

	void								Multiplexer::setReader(int fd)
	{
		Multiplexer::NetworkMap::iterator it;
		it = this->_entries.find(fd);
		if (it != this->_entries.end())
		{
			if (!it->second->_read)
				FD_SET(fd, &this->_readFds);
			it->second->_read = true;
		}
	}

	void								Multiplexer::unsetReader(int fd)
	{
		Multiplexer::NetworkMap::iterator it;
		it = this->_entries.find(fd);
		if (it != this->_entries.end())
		{
			if (it->second->_read)
				FD_CLR(fd, &this->_readFds);
			it->second->_read = false;
		}
	}

	void								Multiplexer::setError(int fd)
	{
		Multiplexer::NetworkMap::iterator it;
		it = this->_entries.find(fd);
		if (it != this->_entries.end())
			FD_SET(fd, &this->_readFds);
	}

	void								Multiplexer::unsetError(int fd)
	{
		Multiplexer::NetworkMap::iterator it;
		it = this->_entries.find(fd);
		if (it != this->_entries.end())
			FD_CLR(fd, &this->_readFds);
	}

// Time event functions

	uint64_t							Multiplexer::setTimeout(uint64_t duration, Timer::Callback f)
	{
		return this->_timer.setHook(duration, f, false);
	}

	uint64_t							Multiplexer::setInterval(uint64_t duration, Timer::Callback f)
	{
		return this->_timer.setHook(duration, f, true);
	}

	bool								Multiplexer::clrTimeout(uint64_t id)
	{
		return this->_timer.clrHook(id);
	}

	bool								Multiplexer::clrInterval(uint64_t id)
	{
		return this->_timer.clrHook(id);
	}
}
