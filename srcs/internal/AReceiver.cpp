#include "internal/AReceiver.hpp"

namespace io
{
// Constructor
	AReceiver::AReceiver(Stream * const stream, size_t buffSize)
	: _stream(stream), _buffSize(buffSize), _callback(0), _readedBytes(0)
	{
		this->_buffer = 0;
		this->setBuffer(new Message);
		this->getBuffer()->resize(buffSize);
	}

// Coplien
	AReceiver::AReceiver(void)
	: _stream(0), _buffSize(AReceiver::DefaultBuffSize), _callback(0), _readedBytes(0)
	{
		this->_buffer = 0;
		this->setBuffer(new Message);
		this->getBuffer()->resize(this->_buffSize);
	}

	AReceiver::~AReceiver(void)
	{
		GC.unlock(this->_buffer);
	}

	AReceiver::AReceiver(AReceiver const & ref)
	: _stream(ref.getStream()), _buffSize(ref.getBuffSize())
	{
		this->_buffer = 0;
		*this = ref;
	}

	AReceiver const & 			AReceiver::operator=(AReceiver const & rhs)
	{
			
		this->setCallback(rhs.getCallback());
		GC.lock(rhs.getBuffer());
		this->setBuffer(rhs.getBuffer());
		this->_readedBytes = rhs.getReadedBytes();
		return *this;
	}

// Getters
	size_t						AReceiver::getBuffSize(void) const
	{
		return this->_buffSize;
	}

	AReceiver::Callback			AReceiver::getCallback(void) const
	{
		return this->_callback;
	}

	Message *					AReceiver::getBuffer(void) const
	{
		return this->_buffer;
	}

	size_t						AReceiver::getReadedBytes(void) const
	{
		return this->_readedBytes;
	}

	Stream * 					AReceiver::getStream(void) const
	{
		return this->_stream;
	}

// Setters

	void						AReceiver::setBuffSize(size_t buffSize)
	{
		this->_buffSize = buffSize;
	}

	void						AReceiver::setBuffer(Message * new_buffer)
	{
		if (this->_buffer)
			GC.unlock(this->_buffer);
		this->_buffer = new_buffer;
	}
	
	void						AReceiver::setCallback(AReceiver::Callback callback)
	{
		this->_callback = callback;
	}

// Get Read Pointer
	char *						AReceiver::getReadPtr(void)
	{
		size_t remainingBytes = this->_buffer->size() - this->_readedBytes;

		if (remainingBytes < this->_buffSize)
			this->_buffer->resize(this->_buffer->size() + this->_buffSize - remainingBytes + 1);
		return &((*this->_buffer)[this->_readedBytes]);
	}

// Sync
	void			AReceiver::sync(void) 
	{
		size_t		rbytes = this->_readedBytes;
		this->_readedBytes = 0;
		this->sync(rbytes);
	}

}
