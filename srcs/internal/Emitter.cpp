#include <unistd.h>
#include <errno.h>

#include "Stream.hpp"
#include "internal/Emitter.hpp"

namespace io
{
// Constructor
	Emitter::Emitter(Stream * const stream, size_t buffSize)
	: _stream(stream), _buffSize(buffSize) {  }

// Coplien
	Emitter::Emitter(void)
	: _stream(0),_buffSize(Emitter::DefaultBuffSize) {  }

	Emitter::~Emitter(void)
	{
		std::queue<Emitter::Data> &q = this->_sendingQueue;
		while (!q.empty())
		{
			if (q.front().msg)
				GC.unlock(q.front().msg);
			q.pop();
		}
	}

	Emitter::Emitter(Emitter const & ref)
	: _stream(ref.getStream())
	{
		*this = ref;
	}

	Emitter const &		Emitter::operator=(Emitter const & rhs)
	{
		this->_buffSize = rhs.getBuffSize();
		this->_sendingQueue = rhs.getSendingQueue();
		return *this;
	}

	// Protected Getters
	Stream *			Emitter::getStream(void) const
	{
		return this->_stream;
	}

	std::queue<Emitter::Data> const &	Emitter::getSendingQueue(void) const
	{
		return this->_sendingQueue;
	}

	// buffSize Getter/Setter
	size_t				Emitter::getBuffSize(void) const
	{
		return this->_buffSize;
	}

	void				Emitter::setBuffSize(size_t buffSize)
	{
		if (!buffSize)
			buffSize = 1;
		this->_buffSize = buffSize;
	}

	// Functions
	bool				Emitter::diffuse(void)
	{
		if (this->_sendingQueue.empty())
			return false;

		Emitter::Data & d = this->_sendingQueue.front();
		size_t len = (d.end - d.cursor < this->_buffSize) ? (d.end - d.cursor) : (this->_buffSize);
		void *ptr = 0;

		if (d.cmsg)
			ptr = const_cast<char *>(d.cmsg + d.cursor);
		else if (d.msg)
			ptr = const_cast<char *>(d.msg->c_str() + d.cursor);
		else
			throw Exception("io::Emitter -> fatal error.");

		if (::write(this->_stream->fdOut(), ptr, len) == -1)
			throw Exception("io::Emitter -> write() error : ", strerror(errno));

		d.cursor += len;
		if (d.cursor >= d.end)
		{
			if (d.callback)
				d.callback(this->_stream);
			if (d.msg)
				GC.unlock(d.msg);
			this->_sendingQueue.pop();
		}

		return (this->_sendingQueue.empty() ? false : true);
	}

	void				Emitter::add(Message *m, size_t start, size_t end, Callback f)
	{
		Emitter::Data d;

		if (!m || (end - start <= 0) || end > m->size())
			return ;

		GC.lock(m);
		d.msg = m;
		d.cursor = start;
		d.end = end;
		d.callback = f;
		d.cmsg = 0;
		this->_sendingQueue.push(d);
	}
	void				Emitter::add(char const *m, size_t start, size_t end, Callback f)
	{
		Emitter::Data d;

		if (!m || (end - start <= 0))
			return ;

		d.cmsg = m;
		d.cursor = start;
		d.end = end;
		d.callback = f;
		d.msg = 0;
		this->_sendingQueue.push(d);
	}

}

