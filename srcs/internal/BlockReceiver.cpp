#include "internal/BlockReceiver.hpp"

namespace io
{
	// Private Clone
	AReceiver *					BlockReceiver::clone(void) const { return new BlockReceiver(*this); }

	// Private Coplien
	BlockReceiver::BlockReceiver(void) : AReceiver(), _blockSize(BlockReceiver::DefaultBlockSize) {  }
	
// Constructor
	BlockReceiver::BlockReceiver(Stream * const stream, size_t const buffSize, size_t blockSize)
	: AReceiver(stream, buffSize), _blockSize(blockSize) {  }

// Coplien
	BlockReceiver::~BlockReceiver(void) {  }
	BlockReceiver::BlockReceiver(BlockReceiver const & ref)
	: AReceiver(ref) { *this = ref; }

	BlockReceiver const &		BlockReceiver::operator=(BlockReceiver const & rhs)
	{
		AReceiver::operator=(rhs);
		this->_blockSize = rhs.getBlockSize();
		return *this;
	}

// blockSize Getter/Setter
	size_t						BlockReceiver::getBlockSize(void) const
	{
		return this->_blockSize;
	}

	void						BlockReceiver::setBlockSize(size_t blockSize)
	{
		if (!blockSize)
			return ;
		this->_blockSize = blockSize;
		AReceiver::sync();
	}

// Sync
	void						BlockReceiver::sync(size_t readedBytes)
	{
		Message * msg;

		this->_readedBytes += readedBytes;
		while (this->_readedBytes >= this->_blockSize)
		{
			if (this->_callback)
			{
				msg = new Message;
				msg->resize(this->_blockSize);
				msg->assign(this->_buffer->begin(), this->_buffer->begin() + this->_blockSize);
				this->_readedBytes -= this->_blockSize;
				this->_callback(this->getStream(), msg);
				GC.unlock(msg);
			}
			else
				this->_readedBytes -= this->_blockSize;
			this->_buffer->erase(this->_buffer->begin(), this->_buffer->begin() + this->_blockSize);
		}
	}

}

