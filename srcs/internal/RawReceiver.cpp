#include "internal/RawReceiver.hpp"

namespace io
{
// Private Clone
	AReceiver *				RawReceiver::clone(void) const { return new RawReceiver(*this); }

// Private Coplien
	RawReceiver::RawReceiver(void) : AReceiver() {  }
	
// Constructor
	RawReceiver::RawReceiver(Stream * const stream, size_t const buffSize)
	: AReceiver(stream, buffSize) {  }

// Coplien
	RawReceiver::~RawReceiver(void) {  }
	RawReceiver::RawReceiver(RawReceiver const & ref)
	: AReceiver(ref) { *this = ref; }

	RawReceiver const &		RawReceiver::operator=(RawReceiver const & rhs)
	{
		AReceiver::operator=(rhs);
		return *this;
	}

// Sync
	void					RawReceiver::sync(size_t readedBytes)
	{
		if (!readedBytes)
			return ;
	// Prepare Message
		Message * msg = this->_buffer;
		msg->resize(readedBytes);

	// Set new buffer
		this->_buffer = new Message;
		this->_buffer->resize(this->getBuffSize());

	// Callback
		if (this->_callback)
			this->_callback(this->getStream(), msg);
		GC.unlock(msg);
		this->_readedBytes = 0;
	}
}
