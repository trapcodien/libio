#include <iostream>
#include <string.h>
#include "internal/Env.hpp"

namespace utils
{
	void	Env::destroy(char **& env)
	{
		if (!env) return;
	
		for (int i = 0; env[i]; i++)
			delete[] env[i];
		delete[] env;
		env = nullptr;
	}
	
	void	Env::copy(char **& dest, char ** const & source)
	{
		Env::destroy(dest);
		if (source)
		{
			size_t size;
			for (size = 0; source[size]; size++) (void)0;
			dest = new char*[size + 1]();
			for (size_t i = 0; i < size; i++)
			{
				dest[i] = new char[strlen(source[i]) + 1]();
				strcpy(dest[i], source[i]);
			}
		}
	}

	char **	Env::create(Env::ParsedEnv const & p_env)
	{
		char ** ret;
		auto size = p_env.size();
		ret = new char*[size + 1]();
	
		int i = 0;
		for (auto it = p_env.begin(); it != p_env.end(); it++)
		{
			std::string str(it->first + "=" + it->second);
			ret[i] = new char[str.size() + 1]();
			strcpy(ret[i], str.c_str());
			i++;
		}
		return ret;
	}
	
	Env::Env(char ** e) : _env(nullptr) { Env::copy(this->_env, e); }
	Env::Env(Env::ParsedEnv const & p_env) : _env(nullptr) { this->setEnv(p_env); }
	Env::Env(void) : _env(nullptr) {  }
	Env::~Env(void) { Env::destroy(this->_env); }
	Env::Env(Env const & ref) { *this = ref; }
	Env const &		Env::operator=(Env const & rhs)
	{
		Env::copy(this->_env, rhs._env);
		return *this;
	}
	
	char * const *		Env::getEnv(void) { return const_cast<char * const *>(this->_env); }
	
	void				Env::setEnv(Env::ParsedEnv const & p_env)
	{
		Env::destroy(this->_env);
		this->_env = Env::create(p_env);
	}
	
	void				Env::debug(void) const
	{
		if (!this->_env) return;
		for (size_t i = 0; this->_env[i]; i++)
			std::cout << this->_env[i] << std::endl;
	}
}
