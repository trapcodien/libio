#include <algorithm>

#include "internal/TextReceiver.hpp"

namespace io
{
// Private Clone
	AReceiver *					TextReceiver::clone(void) const { return new TextReceiver(*this); }

// Private Coplien
	TextReceiver::TextReceiver(void) : AReceiver(), _separator("\n"), _keepSeparator(false) {  }
	
// Constructor
	TextReceiver::TextReceiver(Stream * const stream, size_t const buffSize, std::string const & separator, bool keepSeparator)
	: AReceiver(stream, buffSize), _separator(separator), _keepSeparator(keepSeparator) {  }

// Coplien
	TextReceiver::~TextReceiver(void) {  }
	TextReceiver::TextReceiver(TextReceiver const & ref)
	: AReceiver(ref) { *this = ref; }

	TextReceiver const &		TextReceiver::operator=(TextReceiver const & rhs)
	{
		this->_separator = rhs.getSeparator();
		this->_keepSeparator = rhs.getKeepSeparator();
		AReceiver::operator=(rhs);
		return *this;
	}

// Protected Getters
	std::string const & 	TextReceiver::getSeparator(void) const
	{
		return this->_separator;
	}

	bool					TextReceiver::getKeepSeparator(void) const
	{
		return this->_keepSeparator;
	}

// Sync
	void					TextReceiver::sync(size_t readedBytes)
	{
		std::string const &			sub = this->_separator;
		std::string &				v = *this->_buffer;
		Message						*msg;

		if (!readedBytes)
			return ;
		this->_readedBytes += readedBytes;

		std::string::iterator pos;
		while ((pos = std::search(v.begin(),v.end(),sub.begin(),sub.end())) != v.end())
		{
			this->_readedBytes -= &pos[0] - &v[0] + this->_separator.size();
			if (this->_callback)
			{
				msg = new Message;
				if (this->_keepSeparator)
				{
					msg->resize(pos + this->_separator.size() - v.begin());
					msg->assign(v.begin(), pos + this->_separator.size());
				}
				else
				{
					msg->resize(pos - v.begin());
					msg->assign(v.begin(), pos);
				}
				this->_callback(this->getStream(), msg);
				GC.unlock(msg);
			}
			v.erase(v.begin(), pos + this->_separator.size());
		}
	}
}
