#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>
#include <string.h>

#include "SockStream.hpp"

namespace io
{
// Clone
	SockStream *				SockStream::clone(void) const
	{
		return new SockStream(*this);
	}

// Coplien
	SockStream::SockStream(void) : Stream(-1), _connectCallback(0)
	{
		this->_connected = false;
		this->_port = -1;
		this->_sin.sin_family = AF_INET;
	}

	SockStream::~SockStream(void)
	{
		this->close(io::now);
	}

	SockStream::SockStream(SockStream const & ref) : Stream()
	{
		*this = ref;
	}

	SockStream const &			SockStream::operator=(SockStream const & rhs)
	{
		this->_connected = rhs.isConnected();
		this->_host = rhs.getHost();
		this->_port = rhs.getPort();
		this->_sin = rhs.getSin();
		this->_connectCallback = rhs.getConnectCallback();
		Stream::operator=(rhs);
		return *this;
	}

	SockStream::SockStream(std::string const & host, int port) : Stream(-1), _connectCallback(0)
	{
		this->_connected = false;
		this->_port = -1;
		this->_sin.sin_family = AF_INET;
		this->connect(host, port);
	}

// Getters
	bool 						SockStream::isConnected(void) const
	{
		return this->_connected;
	}

	std::string const &			SockStream::getHost(void) const
	{
		return this->_host;
	}

	int							SockStream::getPort(void) const
	{
		return this->_port;
	}

	sockaddr_in const &			SockStream::getSin(void) const
	{
		return this->_sin;
	}

	SockStream::Callback const &	SockStream::getConnectCallback(void) const
	{
		return this->_connectCallback;
	}

// Functions
	bool						SockStream::connect(std::string const & host, int port)
	{
		struct hostent			*he;
		Multiplexer				*controller;

		controller = this->getController();
		this->close(io::now);
		this->fdIn(socket(AF_INET, SOCK_STREAM, 0));
		if (this->fdIn() == -1)
			throw Exception("io::SockStream -> socket() error : ", strerror(errno));	
		this->fdOut(this->fdIn());
		this->_sin.sin_port = htons(port);

			// Resolve Hostname
		if ((he = gethostbyname(host.c_str())))
			memcpy(&(this->_sin.sin_addr), he->h_addr_list[0], he->h_length);
		else
			this->_sin.sin_addr.s_addr = inet_addr(host.c_str());

			// Connect
		if ((::connect(this->fdIn(), reinterpret_cast<sockaddr *>(&this->_sin), sizeof(sockaddr_in))) == -1)
		{
			::close(this->fdIn());
			this->fdIn(-1);
			this->fdOut(-1);
			return false;
		}
		this->_connected = true;
		this->read(true);
		if (controller)
			controller->plug(this);
		if (this->_connectCallback)
			this->_connectCallback(this);
		return true;
	}

	bool						SockStream::close(Moment m)
	{
		this->_connected = false;
		return Stream::close(m);
	}

	bool						SockStream::close(void)
	{
		this->_connected = false;
		return Stream::close();
	}

	void						SockStream::onReceive(std::function<void(SockStream *, Message *)> f)
	{
		this->receiver()->setCallback(*(reinterpret_cast<AReceiver::Callback * >(&f)));
	}

	void						SockStream::onPlug(SockStream::Callback f)
	{
		this->_plugCallback = (*(reinterpret_cast<Stream::Callback * >(&f)));
	}

	void						SockStream::onUnplug(SockStream::Callback f)
	{
		this->_unplugCallback = (*(reinterpret_cast<Stream::Callback * >(&f)));
	}

	void						SockStream::onClose(SockStream::Callback f)
	{
		this->_closeCallback = (*(reinterpret_cast<Stream::Callback * >(&f)));
	}

	void						SockStream::onConnect(SockStream::Callback f)
	{
		this->_connectCallback = f;
	}

	void						SockStream::send(Message *m, SockStream::Callback f)
	{
		Stream::send(m, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}

	void						SockStream::send(Message *m, size_t len, SockStream::Callback f)
	{
		Stream::send(m, len, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}

	void						SockStream::send(Message *m, size_t start, size_t end, SockStream::Callback f)
	{
		Stream::send(m, start, end, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}

	void						SockStream::send(char const * m, SockStream::Callback f)
	{
		Stream::send(m, (*(reinterpret_cast<Stream::Callback * >(&f))));
	}
}

