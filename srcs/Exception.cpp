#include "Exception.hpp"

namespace io
{
// private Coplien
	Exception::Exception(void) : _msg("Fatal Error in fd namespace.") {  }

// Public Coplien
	Exception::Exception(std::string const & msg, std::string reason) : _msg(msg) { this->_msg += reason; }
	Exception::~Exception(void) throw() {  }
	Exception::Exception(Exception const & ref) { *this = ref; }
	Exception const &		Exception::operator=(Exception const & rhs)
	{
		this->_msg = rhs.getMsg();
		return *this;
	}

// Getter
	std::string const &		Exception::getMsg(void) const { return this->_msg; }

// What Overload
	const char *			Exception::what(void) const throw()
	{
		return this->_msg.c_str();
	}
}

