#include "Garbage.hpp"
#include "Collector.hpp"

static bool		isNew = false;

namespace io
{
	Garbage::Garbage(void) : _onStack(true)
	{
		if (isNew)
			this->_onStack = false;
		isNew = false;
		GC.lock(this);
	}

	Garbage::~Garbage(void)
	{
		GC.forget(this, false);
	}

	void *		Garbage::operator new(size_t size)
	{
		isNew = true;
		return ::operator new(size);
	}

	bool		Garbage::isOnStack(void) const { return this->_onStack; }
}
