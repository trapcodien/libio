# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/08/26 23:49:31 by garm              #+#    #+#              #
#    Updated: 2015/11/21 23:47:00 by garm             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = g++

NAME = libio.a
NAME_TEST = a.out

SOURCES_DIR = srcs
INCLUDES_DIR = includes
SHARED_DIR = /Users/Shared

CHECK = @cppcheck -I includes -I includes/internal --enable=all --check-config --suppress=missingIncludeSystem .
FLAGS = -Wall -Wextra -std=c++11

AUTOCLEAN = @rm -f $(NAME_TEST) ; rm -rf $(NAME_TEST).dSYM
EXPOSED_PORT = 4242

ifeq ($(DEBUG), 1)
	FLAGS += -g3
	VALGRIND = valgrind --dsymutil=yes --leak-check=full
else
	FLAGS += -Werror
endif

ifeq ($(AUTOEXEC), 1)
	EXEC = $(VALGRIND) ./$(NAME_TEST)
endif

ifndef ($(TEST))
	TEST = main.cpp
endif

CFLAGS += $(FLAGS) -I $(INCLUDES_DIR)

DEPENDENCIES = \
				$(INCLUDES_DIR)/internal/Emitter.hpp \
				$(INCLUDES_DIR)/internal/AReceiver.hpp \
				$(INCLUDES_DIR)/internal/RawReceiver.hpp \
				$(INCLUDES_DIR)/internal/TextReceiver.hpp \
				$(INCLUDES_DIR)/internal/BlockReceiver.hpp \
				$(INCLUDES_DIR)/internal/Env.hpp \
				$(INCLUDES_DIR)/io.hpp \
				$(INCLUDES_DIR)/stuff.hpp \
				$(INCLUDES_DIR)/Garbage.hpp \
				$(INCLUDES_DIR)/Collector.hpp \
				$(INCLUDES_DIR)/Exception.hpp \
				$(INCLUDES_DIR)/AAsync.hpp \
				$(INCLUDES_DIR)/Stream.hpp \
				$(INCLUDES_DIR)/SockStream.hpp \
				$(INCLUDES_DIR)/Message.hpp \
				$(INCLUDES_DIR)/Multiplexer.hpp \
				$(INCLUDES_DIR)/Listener.hpp \
				$(INCLUDES_DIR)/FileStream.hpp \
				$(INCLUDES_DIR)/ExecStream.hpp \
				$(INCLUDES_DIR)/Timer.hpp \

SOURCES = \
		  $(SOURCES_DIR)/internal/Emitter.cpp \
		  $(SOURCES_DIR)/internal/AReceiver.cpp \
		  $(SOURCES_DIR)/internal/RawReceiver.cpp \
		  $(SOURCES_DIR)/internal/TextReceiver.cpp \
		  $(SOURCES_DIR)/internal/BlockReceiver.cpp \
		  $(SOURCES_DIR)/internal/Env.cpp \
		  $(SOURCES_DIR)/Garbage.cpp \
		  $(SOURCES_DIR)/Collector.cpp \
		  $(SOURCES_DIR)/Exception.cpp \
		  $(SOURCES_DIR)/AAsync.cpp \
		  $(SOURCES_DIR)/Stream.cpp \
		  $(SOURCES_DIR)/SockStream.cpp \
		  $(SOURCES_DIR)/Multiplexer.cpp \
		  $(SOURCES_DIR)/Listener.cpp \
		  $(SOURCES_DIR)/FileStream.cpp \
		  $(SOURCES_DIR)/ExecStream.cpp \
		  $(SOURCES_DIR)/Timer.cpp \

OBJS = $(SOURCES:.cpp=.o)

all: $(NAME)

submodules_update:
	git submodule init
	git submodule update

%.o: %.cpp $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS)
	@echo Creating $(NAME)...
	@ar rcs $(NAME) $(OBJS)

clean:
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

fclean: cleanbin clean cleandoc
	@rm -f $(NAME_TEST)
	@rm -rf *.dSYM
	@echo Full cleaned.

cleanbin:
	@rm -f $(NAME)
	@rm -rf $(NAME).dSYM
	@echo $(NAME) deleted.

cleanout: cleanbin

check:
	$(CHECK)

cleandoc:
	@rm -rf doc/html
	@echo Doc deleted.

doc:
	@if [ -f Doxyfile ] ; \
	then \
		doxygen ; \
	else \
		echo Doxyfile does not available : checkout on doc branch ; \
	fi;	

opendoc: doc
	open doc/html/index.html

ifeq ($(ENVTEST), 1)
test: fclean init_shared
	@docker run -p $(EXPOSED_PORT):$(EXPOSED_PORT) -itv $(SHARED_DIR)/garm:/home/garm/dev dev make test ENVTEST=0 AUTOEXEC=1 DEBUG=$(DEBUG)
else
test: all
	g++ $(FLAGS) $(TEST) -L . -lio -I includes -o $(NAME_TEST)
	@$(EXEC)
	$(AUTOCLEAN)
endif

docker: fclean init_shared
	@docker run -itv $(SHARED_DIR)/garm:/home/garm/dev dev

init_shared:
	@echo Initialize $(SHARED_DIR)/garm
	@rm -rf $(SHARED_DIR)/garm
	@cp -R . /Users/Shared/garm

re: fclean all

.PHONY: clean fclean re all doc cleandoc test docker init_shared submodules_update

