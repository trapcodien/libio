#include "io.hpp"

int		main(void)
{
	io::Multiplexer		multi;			// We always need it for enter in the Asynchronous world with libIO.
	io::Stream			keyboard(0);	// 0 is the stdin file descriptor.

		// Set keyboard receive event
	keyboard.onReceive([](io::Stream * keyboard, io::Message * msg) {
		if (*msg == "exit")	// Remember io::Message is a std::string
			keyboard->unplug();
		else	
			std::cout << *msg << std::endl;
	});

		// Set keyboard plug event
	keyboard.onPlug([](io::Stream * keyboard){
		std::cout << "--- Keyboard Plugged ---" << std::endl;
		(void) keyboard;
	});

		// Set keyboard unplug event
	keyboard.onUnplug([](io::Stream * keyboard){
		std::cout << "--- Keyboard Unplugged ---" << std::endl;
		(void) keyboard;
	});

		// Set keyboard close event (this will happen when user press CTRL+D for example)
		// While testing, as we can see, keyboard onUnplug event is called before keyboard onClose event
	keyboard.onClose([](io::Stream * keyboard){
		std::cout << "--- STDIN CLOSE ---" << std::endl;
		(void) keyboard;
	});


	multi.plug(&keyboard); 				// Watch keyboard events.
	while (multi)						// While Multiplexer has plugged objects.
		multi.ioPoll(); 				// Blocking I/O poll.

	return (0);
}

