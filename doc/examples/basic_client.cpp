#include <stdlib.h>
#include <iostream>
#include "io.hpp"

// Before launch this test, please start a listening netcat server with :  'nc -l -p 4242'
int		main(void)
{
	io::Multiplexer		multi;
	io::SockStream		remote_client; 	// SockStream is a Stream but with TCP support.
	io::Stream			keyboard(0);

		// Connect a SockStream is easy, please refer to the reference.
	if (!remote_client.connect("127.0.0.1", 4242))
	{
		std::cout << "Error: Unable to connect to '127.0.0.1:4242'" << std::endl;
		return (0); 	// return 0 for Makefile success
	}

	remote_client.onReceive([](io::SockStream * sock, io::Message * msg) {
		if (*msg == "exit") // Warning, by default, separator is not keeped
			sock->getController()->unplugAll(); //	This is a way for exit IO loop
		else
			std::cout << *msg << std::endl;
	});

	keyboard.onReceive([&remote_client](io::Stream * keyboard, io::Message * msg) {
		if (*msg == "exit\n") // Separator has keeped, so we compare \n too
			keyboard->unplug();
		else
			remote_client.send(msg);	// Please see io::Stream::send() documentation
	});

	keyboard.onUnplug([&remote_client](io::Stream * keyboard) {
		remote_client.close();
		(void) keyboard;
	});

	keyboard.textMode("\n", true); 		// Here we keep the separator (see reference)

	multi.plug(&keyboard);
	multi.plug(&remote_client);
	while (multi)
		multi.ioPoll();
	return (0);
}

