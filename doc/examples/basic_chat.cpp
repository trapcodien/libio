#include <iostream>
#include <list>
#include "io.hpp"

int		main(void)
{
	typedef std::list<io::SockStream *> Connections;

	io::Multiplexer				multi;
	io::Listener				server(4242);
	io::SockStream				model;
	Connections					clients;  				// Keep clients pointer in a list

	model.onConnect([&clients](io::SockStream * client) {
		clients.push_back(client); 						// Update list
		std::cout << "--- NEW CLIENT ---" << std::endl;
	});

	model.onClose([&clients](io::SockStream * client) { // Update list
		clients.erase(std::find(clients.begin(), clients.end(), client));
		std::cout << "--- CLIENT LEFT ---" << std::endl;
	});

	model.onReceive([&clients](io::SockStream * client, io::Message * msg) {
		if (*msg == "exit")
			client->getController()->unplugAll();		// Exit loop if someone send 'exit'
		else if (*msg != "")							// Don't broadcast if message is empty
		{
			for (Connections::iterator it = clients.begin() ; it != clients.end() ; ++it)
			{
				if (*it != client)
				{
					(*it)->send("-> ");
					(*it)->send(msg);
					(*it)->send("\n");
				}
			}
		}
	});

	server.setModel(&model);
	multi.plug(&server);
	while (multi)
		multi.ioPoll();

	return (0);
}

