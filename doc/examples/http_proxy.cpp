#include <iostream>
#include <sstream>
#include <list>
#include "io.hpp"

int		getPort(std::string const & host)
{
	size_t				pos;
	std::stringstream	ss;
	int					port;

	if ((pos = host.find(":")) == host.npos)
		return 80;
	pos++;
	ss << &host[pos];
	ss >> port;
	return port;
}

int		main(void)
{
	try
	{
	io::Multiplexer					multi;
	io::Listener					server(4242);
	io::SockStream					model;
	io::Stream						keyboard(0);

	keyboard.onReceive([&multi](io::Stream *s, io::Message *m){
		if (m->find("exit") != m->npos)
			multi.unplugAll();
		(void)s;
	});

	model.onConnect([](io::SockStream *s){
		s->textMode("\r\n\r\n", true);
		std::cout << "[CONNECTED] " << s->getHost() << ":" << s->getPort() << std::endl;
	});

	model.onClose([](io::SockStream *s){
		std::cout << "[DISCONNECTED] " << s->getHost() << ":" << s->getPort() << std::endl;
		
	});

	model.onReceive([&](io::SockStream *s, io::Message *m){
		size_t start;
		io::Message host;

		if ((start = m->find("Host: ")) != m->npos)
		{
			size_t	end;
			int			port;
			start += 6;
			end = m->find("\r\n", start);
			host.assign(*m, start, end - start);
			port = getPort(host);
			host.assign(host, 0, host.find(":"));
			std::cout << host << std::endl;
			std::cout << port << std::endl;

			io::SockStream * connection = new io::SockStream;
			connection->rawMode();
			connection->onClose([s](io::SockStream *sock){
				s->close();
				std::cout << "REMOTE CLOSED." << std::endl;
				(void)sock;
			});
			connection->onReceive([s](io::SockStream *sock, io::Message *msg){
				s->send(msg);
				(void)sock;
			});

			if (connection->connect(host, port))
			{
				multi.plug(connection);
				std::cout << "REMOTE CONNECTED" << std::endl;
				connection->send(m);

				s->onClose([connection](io::SockStream *sock){
					connection->close();
					std::cout << "[DISCONNECTED] " << sock->getHost() << ":" << sock->getPort() << std::endl;
					(void) sock;
				});
				s->onReceive([connection](io::SockStream *sock, io::Message *msg){
					connection->send(msg);
					(void)sock;
				});
				s->rawMode();
			}
			else
			{
				std::cout << "ERROR CONNECTION" << std::endl;
				delete connection;
			}
		}
		(void)s;
	});


	server.setModel(&model);

	multi.plug(&server);
	multi.plug(&keyboard);

	while (multi)
		multi.ioPoll();
	}
	catch (io::Exception const & e)
	{
		std::cout << e.what() << std::endl;
	}
	return 0;
}
