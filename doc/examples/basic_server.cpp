#include <iostream>
#include "io.hpp"

int		main(void)
{
	io::Multiplexer		multi;
	io::Stream			keyboard(0);
	io::Listener		server(4242); 	// Listen on port 4242
	io::SockStream		model;			// Listener need a model for create new TCP connections

	model.onReceive([](io::SockStream * model, io::Message * msg) {
		if (*msg == "exit")
			model->close();
		else
			std::cout << *msg << std::endl;
	});

	model.onConnect([](io::SockStream * model) {
		std::cout << "--- NEW CLIENT ---" << std::endl;
		(void) model;
	});

	model.onClose([](io::SockStream * model) {
		std::cout << "--- CLIENT LEFT ---" << std::endl;
		(void) model;
	});

	server.setModel(&model);
	multi.plug(&server); 			// Don't plug the model, juste the Listener
	multi.plug(&server); 			// Don't plug the model, juste the Listener
	multi.plug(&keyboard);

	// Note : Callbacks can be set and reset anywhere
	keyboard.onReceive([&server, &multi](io::Stream * keyboard, io::Message * msg) {
		if (*msg == "start")
		{
			server.listen(4242);
			multi.plug(&server); 	// We nee to replug the Listener
		}
		else if (*msg == "stop")
			server.stopListen(); 	// Warning : stopListen will unplug the Listener
		else if (*msg == "exit")
			keyboard->getController()->unplugAll();
	});

	while (multi)
		multi.ioPoll();

	return (0);
}

