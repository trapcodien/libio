#include <unistd.h>
#include <iostream>
#include "io.hpp"
#include <list>

io::ExecStream * letest(void)
{
	io::ExecStream		*process = new io::ExecStream("/bin/ls", "ls -R /etc", nullptr);
	process->onExit([](io::ExecStream * p, int exitcodes, bool signaled) {
		if (signaled)
			std::cout << "------------ SIGNAL RECEIVED, EXIT PROCESS ---------------" << std::endl;
		else
			std::cout << "--- EXIT : " << exitcodes << std::endl;
		(void)exitcodes;
		(void)p;
		(void)signaled;
	});

	process->onUnplug([](io::ExecStream * p){
			std::cout << "--- UNPLUGGED" << std::endl;
			(void)p;
	});

	process->onPlug([](io::ExecStream * p){
			std::cout << "--- PLUGGED" << std::endl;
			(void)p;
	});

	process->onReceive([](io::ExecStream * process, io::Message *msg) {	
		std::cout << *msg << std::flush;
		(void)process;
		(void)msg;
	});

	process->onErrReceive([](io::ExecStream * process, io::Message *msg) {	
		std::cout << "ERROR :" << *msg << std::flush;
		(void)process;
		(void)msg;
	});

	process->onFork([](io::ExecStream * process) {
		(void) process;
	});

	process->onClose([](io::ExecStream * p) {
		std::cout << "--- CLOSED" << std::endl;
		(void)p;
	});
	return process;
}


int		main(int argc, char **argv, char **envp)
{
	try {
	io::Multiplexer		multi;
	io::Stream			keyboard(0);
	io::Listener		server(4242); 	// Listen on port 4242
	io::SockStream		model;			// Listener need a model for create new TCP connections
	io::ExecStream		*process = letest();

	std::list<io::SockStream *>		clients;

	model.onReceive([&process](io::SockStream * model, io::Message * msg) {
		if (*msg == "exit")
			model->close();
		else
			std::cout << *msg << std::flush;
	});

	model.onConnect([&clients](io::SockStream * model) {
		clients.push_back(model);
		std::cout << "--- NEW CLIENT ---" << std::endl;
		(void) model;
	});

	model.onClose([&clients](io::SockStream * model) {
		clients.remove(model);
		std::cout << "--- CLIENT LEFT ---" << std::endl;
		(void) model;
	});


	server.setModel(&model);
	multi.plug(&server); 			// Don't plug the model, juste the Listener
	//multi.plug(&server); 			// Don't plug the model, juste the Listener
	multi.plug(&keyboard);
	

	// Note : Callbacks can be set and reset anywhere
	keyboard.onReceive([&process, &clients, &server, &multi](io::Stream * keyboard, io::Message * msg) {
		static int id = -1;
		if (*msg == "start")
		{
			std::cout << "START CMD" << std::endl;
			for (auto it = clients.begin(); it != clients.end(); it++)
				(*it)->read(true);
			
		}
		else if (*msg == "stop")
		{
			std::cout << "STOP CMD" << std::endl;
			for (auto it = clients.begin(); it != clients.end(); it++)
				(*it)->read(false);
		}
		else if (*msg == "exit")
			keyboard->getController()->unplugAll();
		else if (*msg == "exec")
		{
			process->exec();
			multi.plug(process);
		}
		else if (*msg == "kill")
			process->kill();
		else if (*msg == "tempo")
		{
			std::cout << "Hello World in 4 seconds..." << std::endl;
			multi.setTimeout(4000, [](uint64_t id) {
				std::cout << "Hello World !" << std::endl;
				(void) id;
			});
		}
		else if (*msg == "start_interval" && id == -1)
		{
			id = multi.setInterval(1000, [](uint64_t id) {
				std::cout << "YOLO INTERVAL" << std::endl;
				(void)id;
			});
		}
		else if (*msg == "stop_interval" && id != -1)
		{
			multi.clrInterval(id);
			id = -1;
		}
	});

	while (multi)
	{
		multi.ioPoll();
	}
	} catch(io::Exception & e)
	{
		std::cout << e.what() << std::endl;
	}

	return (0);
	(void)argc;
	(void)argv;
	(void)envp;
}

