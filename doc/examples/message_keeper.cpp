#include <queue>
#include "io.hpp"

int		main(void)
{
	std::queue<io::Message *>	msgQueue;
	io::Multiplexer				multi;
	io::Stream					keyboard(0);

		// Here we capture msgQueue by reference ( see c++11 documentation )
	keyboard.onReceive([&msgQueue](io::Stream * keyboard, io::Message * msg) {
		if (*msg == "exit")
			keyboard->unplug();
		else
		{
			io::GC.lock(msg);		//Here we use Global Collector object for keep message.
			msgQueue.push(msg); 	// Keep message.
		}
	});

	multi.plug(&keyboard);
	while (multi)
		multi.ioPoll(); 			// Blocking poll

	// Out of the loop

	while (!msgQueue.empty())
	{
		std::cout << *msgQueue.front() << std::endl; 	// Display Message
		io::GC.unlock(msgQueue.front()); 				// Release it
		msgQueue.pop();
	}
	return (0);
}

