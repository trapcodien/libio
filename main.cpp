#include <unistd.h>
#include <iostream>
#include <list>

#include "io.hpp"

int		main(int argc, char **argv, char **envp)
{
	try {
	io::Multiplexer		multi;
	io::Stream			keyboard(0);
	io::Listener		server(4242); 	// Listen on port 4242
	io::SockStream		model;			// Listener need a model for create new TCP connections

	std::list<io::SockStream *>		clients;

	model.onReceive([](io::SockStream * model, io::Message * msg) {
		if (*msg == "exit")
			model->close();
		else
			std::cout << *msg << std::flush;
	});

	model.onConnect([&clients](io::SockStream * model) {
		clients.push_back(model);
		std::cout << "--- NEW CLIENT ---" << std::endl;
		(void) model;
	});

	model.onClose([&clients](io::SockStream * model) {
		clients.remove(model);
		std::cout << "--- CLIENT LEFT ---" << std::endl;
		(void) model;
	});


	server.setModel(&model);
	multi.plug(&server); 			// Don't plug the model, juste the Listener
	multi.plug(&keyboard);
	

	// Note : Callbacks can be set and reset anywhere
	keyboard.onReceive([&clients, &server, &multi](io::Stream * keyboard, io::Message * msg) {
		if (*msg == "exit")
			keyboard->getController()->unplugAll();
	});

	while (multi)
	{
		multi.ioPoll();
	}
	} catch(io::Exception & e)
	{
		std::cout << e.what() << std::endl;
	}

	return (0);
	(void)argc;
	(void)argv;
	(void)envp;
}

