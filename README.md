LibIO Documentation
===================

- ``` git clone https://trapcodien@bitbucket.org/trapcodien/libio.git ```

- ``` cd libio && make doc ``` (you need to install doxygen)

- Then, open ./doc/html/index.html generated file.